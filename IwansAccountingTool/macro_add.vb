﻿Public Class macro_add
    Public macrostring As String
    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton1.Click
        If Not mc_name.Text = "" Then
            macrostring = "<M>" & mc_name.Text & vbNewLine & macrostring
            MainF.macro.AddRange(macrostring.Split(vbNewLine))
            MainF.refreshmacros()
            Me.Close()
        Else
            MsgBox("Name cannot be blank.", MsgBoxStyle.OkOnly, "Errr...")
        End If
    End Sub
End Class