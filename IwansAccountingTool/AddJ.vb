﻿Public Class AddJ
    Public editmode As Boolean = False
    Public sender As String
    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub AddJ_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        refreshac()
        If editmode = False Then
            l_j.Text = "Journal No: " & MainF.jcount.ToString("D4")
            k_open.Visible = True
        Else
            k_open.Visible = False
        End If
    End Sub
    Sub refreshac()
        cbd.Items.Clear()
        cbc.Items.Clear()
        cbc.Items.AddRange(MainF.acclist.ToArray)
        cbd.Items.AddRange(MainF.acclist.ToArray)
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If (t_amt_d.Text = "" Or t_amt_d.Text = "0" Or t_amt_d.Text = "0.00" Or cbd.Text = "") Then
            MsgBox("Amount cannot be blank nor 0;" & vbNewLine & "Account name cannot be blank.", MsgBoxStyle.OkOnly, "Errr...")
            Exit Sub
        Else
            If MainF.acclist.Contains(cbd.Text) Then
                dg_d.Rows.Add(New String() {cbd.Text, Convert.ToDecimal(New DataTable().Compute(t_amt_d.Text, Nothing)).ToString("F2")})
                cbd.Text = ""
                t_amt_d.Text = ""
            Else
                If MsgBox("The account does not exist yet, will you like to define it?", MsgBoxStyle.OkCancel, "Errr...") = MsgBoxResult.Ok Then
                    addacc.Show()
                    addacc.snd = 1
                    addacc.def(cbd.Text)
                End If
            End If
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If (t_amt_c.Text = "" Or t_amt_c.Text = "0" Or t_amt_c.Text = "0.00" Or cbc.Text = "") Then
            MsgBox("Amount cannot be blank nor 0;" & vbNewLine & "Account name cannot be blank.", MsgBoxStyle.OkOnly, "Errr...")
            Exit Sub
        Else
            If MainF.acclist.Contains(cbc.Text) Then
                dg_c.Rows.Add(New String() {cbc.Text, Convert.ToDecimal(New DataTable().Compute(t_amt_c.Text, Nothing)).ToString("F2")})
                cbc.Text = ""
                t_amt_c.Text = ""
            Else
                If MsgBox("The account does not exist yet, will you like to define it?", MsgBoxStyle.OkCancel, "Errr...") = MsgBoxResult.Ok Then
                    addacc.Show()
                    addacc.snd = 2
                    addacc.def(cbc.Text)
                End If
            End If
        End If
    End Sub
    Function returnbal(ByVal z As Integer)
        If z = 1 Then
            Dim rd As Decimal
            For i As Integer = 0 To dg_d.RowCount - 1
                rd += dg_d.Rows(i).Cells(1).Value
            Next
            Return rd
        ElseIf z = 2 Then
            Dim rc As Decimal
            For i As Integer = 0 To dg_c.RowCount - 1
                rc += dg_c.Rows(i).Cells(1).Value
            Next
            Return rc
        End If
    End Function
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim rd As Decimal = returnbal(1)
        Dim rc As Decimal = returnbal(2)
        If (rd = 0 And rc = 0) Then
            MsgBox("No transactions.", MsgBoxStyle.OkOnly, "Errr...")
        Else
            If rd = rc Then
                If editmode = False Then
                    feedentry()
                Else
                    editentry()
                End If
            Else
                MsgBox("The amount in Debit has to be equal to the amount in Credit", MsgBoxStyle.OkOnly, "Errr...")
            End If
        End If

    End Sub
    Sub editentry()
        For Each item As journals In MainF.field.Controls.OfType(Of journals)
            If item.Name = sender Then
                item.dg.Rows.Clear()
                For i As Integer = 0 To dg_d.RowCount - 1
                    item.dg.Rows.Add(New String() {dtp.Text, dg_d.Rows(i).Cells(0).Value, dg_d.Rows(i).Cells(1).Value, ""})
                Next
                For i As Integer = 0 To dg_c.RowCount - 1
                    item.dg.Rows.Add(New String() {dtp.Text, dg_c.Rows(i).Cells(0).Value, "", dg_c.Rows(i).Cells(1).Value})
                Next
                If Not t_desc.Text = "" Then item.dg.Rows.Add(New String() {"", "(" & t_desc.Text & ")"})
                item.dg.ClearSelection()
                If MainF.t_netw.Enabled = True Then editoldentry(item.dumpcode)
            End If
        Next
        Me.Close()
    End Sub
    Sub feedentry()
        Dim myuc As New journals
        myuc.Dock = DockStyle.Top
        For i As Integer = 0 To dg_d.RowCount - 1
            myuc.dg.Rows.Add(New String() {dtp.Text, dg_d.Rows(i).Cells(0).Value, dg_d.Rows(i).Cells(1).Value, ""})
        Next
        For i As Integer = 0 To dg_c.RowCount - 1
            myuc.dg.Rows.Add(New String() {dtp.Text, dg_c.Rows(i).Cells(0).Value, "", dg_c.Rows(i).Cells(1).Value})
        Next
        If Not t_desc.Text = "" Then myuc.dg.Rows.Add(New String() {"", "(" & t_desc.Text & ")"})
        myuc.JNo.Text = l_j.Text
        myuc.Name = MainF.jcount.ToString("D4")
        MainF.field.Controls.Add(myuc)
        myuc.dg.ClearSelection()
        MainF.field.Controls.SetChildIndex(myuc, 0)
        MainF.jcount += 1
        If MainF.t_netw.Enabled = True Then updatenewentry(myuc.dumpcode)
        If Not k_open.Checked = True Then
            Me.Close()
        Else
            l_j.Text = "Journal No: " & MainF.jcount.ToString("D4")
            cbd.Text = ""
            cbc.Text = ""
            t_amt_c.Clear()
            t_amt_d.Clear()
            dg_d.Rows.Clear()
            dg_c.Rows.Clear()
            t_desc.Clear()
        End If
    End Sub

    Private Sub t_amt_d_KeyPress(sender As Object, e As KeyPressEventArgs) Handles t_amt_d.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            If Not t_amt_d.Text = "" Then
                e.Handled = True
                Button1.PerformClick()
            Else
                e.Handled = True
                Dim dcdb As Decimal = returnbal(2) - returnbal(1)
                t_amt_d.Text = dcdb.ToString("F2")
            End If
        End If
    End Sub

    Private Sub t_amt_c_KeyPress(sender As Object, e As KeyPressEventArgs) Handles t_amt_c.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            If Not t_amt_c.Text = "" Then
                e.Handled = True
                Button2.PerformClick()
            Else
                e.Handled = True
                Dim dbdc As Decimal = returnbal(1) - returnbal(2)
                t_amt_c.Text = dbdc.ToString("F2")
            End If
        End If
    End Sub

    Private Sub cbd_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cbd.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            t_amt_d.Select()
        ElseIf e.KeyChar = ChrW(Keys.Right) Then
            e.Handled = True
            cbc.Select()
        End If
    End Sub

    Private Sub cbc_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cbc.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            t_amt_c.Select()
        ElseIf e.KeyChar = ChrW(Keys.Left) Then
            e.Handled = True
            cbd.Select()
        End If
    End Sub

    Private Sub k_open_CheckedChanged(sender As Object, e As EventArgs) Handles k_open.CheckedChanged

    End Sub

    Private Sub t_desc_KeyPress(sender As Object, e As KeyPressEventArgs) Handles t_desc.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            Button3.PerformClick()
        End If
    End Sub

    Private Sub SenderToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SenderToolStripMenuItem.Click
        Dim myItem As ToolStripMenuItem = CType(sender, ToolStripMenuItem)
        Dim cms As ContextMenuStrip = CType(myItem.Owner, ContextMenuStrip)
        If cms.SourceControl.Name = "dg_d" Then
            If Not dg_d.SelectedRows.Count = 0 Then dg_d.Rows.Remove(dg_d.SelectedRows(0))
        ElseIf cms.SourceControl.Name = "dg_c" Then
            If Not dg_c.SelectedRows.Count = 0 Then dg_c.Rows.Remove(dg_c.SelectedRows(0))
        End If
    End Sub
End Class