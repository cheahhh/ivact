﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class journals
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.JNo = New System.Windows.Forms.Label()
        Me.cms = New MaterialSkin.Controls.MaterialContextMenuStrip()
        Me.EditToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreateMacroToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.dg = New System.Windows.Forms.DataGridView()
        Me.ddate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.accs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.deb = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cred = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cms.SuspendLayout()
        CType(Me.dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'JNo
        '
        Me.JNo.AutoSize = True
        Me.JNo.ContextMenuStrip = Me.cms
        Me.JNo.Dock = System.Windows.Forms.DockStyle.Top
        Me.JNo.Location = New System.Drawing.Point(0, 0)
        Me.JNo.Name = "JNo"
        Me.JNo.Size = New System.Drawing.Size(39, 13)
        Me.JNo.TabIndex = 0
        Me.JNo.Text = "J-0000"
        '
        'cms
        '
        Me.cms.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.cms.Depth = 0
        Me.cms.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EditToolStripMenuItem1, Me.RemoveToolStripMenuItem1, Me.CreateMacroToolStripMenuItem1})
        Me.cms.MouseState = MaterialSkin.MouseState.HOVER
        Me.cms.Name = "cms"
        Me.cms.Size = New System.Drawing.Size(153, 92)
        '
        'EditToolStripMenuItem1
        '
        Me.EditToolStripMenuItem1.Name = "EditToolStripMenuItem1"
        Me.EditToolStripMenuItem1.Size = New System.Drawing.Size(152, 22)
        Me.EditToolStripMenuItem1.Text = "Edit"
        '
        'RemoveToolStripMenuItem1
        '
        Me.RemoveToolStripMenuItem1.Name = "RemoveToolStripMenuItem1"
        Me.RemoveToolStripMenuItem1.Size = New System.Drawing.Size(152, 22)
        Me.RemoveToolStripMenuItem1.Text = "Remove"
        '
        'CreateMacroToolStripMenuItem1
        '
        Me.CreateMacroToolStripMenuItem1.Name = "CreateMacroToolStripMenuItem1"
        Me.CreateMacroToolStripMenuItem1.Size = New System.Drawing.Size(152, 22)
        Me.CreateMacroToolStripMenuItem1.Text = "Create Macro"
        '
        'dg
        '
        Me.dg.AllowUserToAddRows = False
        Me.dg.AllowUserToDeleteRows = False
        Me.dg.AllowUserToResizeColumns = False
        Me.dg.AllowUserToResizeRows = False
        Me.dg.BackgroundColor = System.Drawing.Color.White
        Me.dg.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ddate, Me.accs, Me.deb, Me.cred})
        Me.dg.ContextMenuStrip = Me.cms
        Me.dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dg.GridColor = System.Drawing.Color.White
        Me.dg.Location = New System.Drawing.Point(0, 13)
        Me.dg.Name = "dg"
        Me.dg.ReadOnly = True
        Me.dg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg.Size = New System.Drawing.Size(615, 112)
        Me.dg.TabIndex = 1
        '
        'ddate
        '
        Me.ddate.FillWeight = 115.0!
        Me.ddate.HeaderText = "Date"
        Me.ddate.Name = "ddate"
        Me.ddate.ReadOnly = True
        Me.ddate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ddate.Width = 115
        '
        'accs
        '
        Me.accs.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.accs.HeaderText = "Account Titles and Explanation"
        Me.accs.Name = "accs"
        Me.accs.ReadOnly = True
        '
        'deb
        '
        Me.deb.HeaderText = "Debit"
        Me.deb.Name = "deb"
        Me.deb.ReadOnly = True
        '
        'cred
        '
        Me.cred.HeaderText = "Credit"
        Me.cred.Name = "cred"
        Me.cred.ReadOnly = True
        '
        'journals
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ContextMenuStrip = Me.cms
        Me.Controls.Add(Me.dg)
        Me.Controls.Add(Me.JNo)
        Me.Name = "journals"
        Me.Size = New System.Drawing.Size(615, 125)
        Me.cms.ResumeLayout(False)
        CType(Me.dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents JNo As Label
    Friend WithEvents dg As DataGridView
    Friend WithEvents ddate As DataGridViewTextBoxColumn
    Friend WithEvents accs As DataGridViewTextBoxColumn
    Friend WithEvents deb As DataGridViewTextBoxColumn
    Friend WithEvents cred As DataGridViewTextBoxColumn
    Friend WithEvents cms As MaterialSkin.Controls.MaterialContextMenuStrip
    Friend WithEvents EditToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents RemoveToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents CreateMacroToolStripMenuItem1 As ToolStripMenuItem
End Class
