﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class settings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.t_file = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.MaterialFlatButton1 = New MaterialSkin.Controls.MaterialFlatButton()
        Me.MaterialRaisedButton1 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.MaterialFlatButton3 = New MaterialSkin.Controls.MaterialFlatButton()
        Me.MaterialDivider1 = New MaterialSkin.Controls.MaterialDivider()
        Me.c_ne = New MaterialSkin.Controls.MaterialCheckBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.c_part = New MaterialSkin.Controls.MaterialCheckBox()
        Me.dg = New System.Windows.Forms.DataGridView()
        Me.pip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.stats = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MaterialContextMenuStrip1 = New MaterialSkin.Controls.MaterialContextMenuStrip()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.AddPeerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemovePeerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MaterialRaisedButton2 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.l_myip = New MaterialSkin.Controls.MaterialLabel()
        CType(Me.dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MaterialContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        't_file
        '
        Me.t_file.Depth = 0
        Me.t_file.Hint = "Default File"
        Me.t_file.Location = New System.Drawing.Point(12, 9)
        Me.t_file.MaxLength = 32767
        Me.t_file.MouseState = MaterialSkin.MouseState.HOVER
        Me.t_file.Name = "t_file"
        Me.t_file.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.t_file.SelectedText = ""
        Me.t_file.SelectionLength = 0
        Me.t_file.SelectionStart = 0
        Me.t_file.Size = New System.Drawing.Size(200, 23)
        Me.t_file.TabIndex = 1
        Me.t_file.TabStop = False
        Me.t_file.UseSystemPasswordChar = False
        '
        'MaterialFlatButton1
        '
        Me.MaterialFlatButton1.AutoSize = True
        Me.MaterialFlatButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.MaterialFlatButton1.Depth = 0
        Me.MaterialFlatButton1.Location = New System.Drawing.Point(219, 1)
        Me.MaterialFlatButton1.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.MaterialFlatButton1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialFlatButton1.Name = "MaterialFlatButton1"
        Me.MaterialFlatButton1.Primary = False
        Me.MaterialFlatButton1.Size = New System.Drawing.Size(23, 36)
        Me.MaterialFlatButton1.TabIndex = 2
        Me.MaterialFlatButton1.Text = "..."
        Me.MaterialFlatButton1.UseVisualStyleBackColor = True
        '
        'MaterialRaisedButton1
        '
        Me.MaterialRaisedButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MaterialRaisedButton1.Depth = 0
        Me.MaterialRaisedButton1.Location = New System.Drawing.Point(204, 435)
        Me.MaterialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton1.Name = "MaterialRaisedButton1"
        Me.MaterialRaisedButton1.Primary = True
        Me.MaterialRaisedButton1.Size = New System.Drawing.Size(68, 29)
        Me.MaterialRaisedButton1.TabIndex = 3
        Me.MaterialRaisedButton1.Text = "Save"
        Me.MaterialRaisedButton1.UseVisualStyleBackColor = True
        '
        'MaterialFlatButton3
        '
        Me.MaterialFlatButton3.AutoSize = True
        Me.MaterialFlatButton3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.MaterialFlatButton3.Depth = 0
        Me.MaterialFlatButton3.Location = New System.Drawing.Point(250, 1)
        Me.MaterialFlatButton3.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.MaterialFlatButton3.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialFlatButton3.Name = "MaterialFlatButton3"
        Me.MaterialFlatButton3.Primary = False
        Me.MaterialFlatButton3.Size = New System.Drawing.Size(21, 36)
        Me.MaterialFlatButton3.TabIndex = 4
        Me.MaterialFlatButton3.Text = "X"
        Me.MaterialFlatButton3.UseVisualStyleBackColor = True
        '
        'MaterialDivider1
        '
        Me.MaterialDivider1.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialDivider1.Depth = 0
        Me.MaterialDivider1.Location = New System.Drawing.Point(12, 38)
        Me.MaterialDivider1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialDivider1.Name = "MaterialDivider1"
        Me.MaterialDivider1.Size = New System.Drawing.Size(259, 2)
        Me.MaterialDivider1.TabIndex = 5
        Me.MaterialDivider1.Text = "MaterialDivider1"
        '
        'c_ne
        '
        Me.c_ne.AutoSize = True
        Me.c_ne.Depth = 0
        Me.c_ne.Font = New System.Drawing.Font("Roboto", 10.0!)
        Me.c_ne.Location = New System.Drawing.Point(9, 43)
        Me.c_ne.Margin = New System.Windows.Forms.Padding(0)
        Me.c_ne.MouseLocation = New System.Drawing.Point(-1, -1)
        Me.c_ne.MouseState = MaterialSkin.MouseState.HOVER
        Me.c_ne.Name = "c_ne"
        Me.c_ne.Ripple = True
        Me.c_ne.Size = New System.Drawing.Size(134, 30)
        Me.c_ne.TabIndex = 6
        Me.c_ne.Text = "Network Enabled"
        Me.ToolTip1.SetToolTip(Me.c_ne, "If enabled, the program will start listening on port 52347. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "This program utiliz" &
        "es P2P networking and" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Blockchain technology.")
        Me.c_ne.UseVisualStyleBackColor = True
        '
        'c_part
        '
        Me.c_part.AutoSize = True
        Me.c_part.Depth = 0
        Me.c_part.Font = New System.Drawing.Font("Roboto", 10.0!)
        Me.c_part.Location = New System.Drawing.Point(9, 73)
        Me.c_part.Margin = New System.Windows.Forms.Padding(0)
        Me.c_part.MouseLocation = New System.Drawing.Point(-1, -1)
        Me.c_part.MouseState = MaterialSkin.MouseState.HOVER
        Me.c_part.Name = "c_part"
        Me.c_part.Ripple = True
        Me.c_part.Size = New System.Drawing.Size(108, 30)
        Me.c_part.TabIndex = 10
        Me.c_part.Text = "Participating"
        Me.ToolTip1.SetToolTip(Me.c_part, "Participating computers will receive updates to the file" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "instead of just documen" &
        "ting the transactions.")
        Me.c_part.UseVisualStyleBackColor = True
        '
        'dg
        '
        Me.dg.AllowUserToAddRows = False
        Me.dg.AllowUserToDeleteRows = False
        Me.dg.AllowUserToResizeColumns = False
        Me.dg.AllowUserToResizeRows = False
        Me.dg.BackgroundColor = System.Drawing.Color.White
        Me.dg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.pip, Me.stats})
        Me.dg.ContextMenuStrip = Me.MaterialContextMenuStrip1
        Me.dg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dg.GridColor = System.Drawing.Color.White
        Me.dg.Location = New System.Drawing.Point(9, 128)
        Me.dg.MultiSelect = False
        Me.dg.Name = "dg"
        Me.dg.ReadOnly = True
        Me.dg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg.Size = New System.Drawing.Size(262, 301)
        Me.dg.TabIndex = 7
        '
        'pip
        '
        Me.pip.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.pip.HeaderText = "Peer"
        Me.pip.Name = "pip"
        Me.pip.ReadOnly = True
        '
        'stats
        '
        Me.stats.HeaderText = "Status"
        Me.stats.Name = "stats"
        Me.stats.ReadOnly = True
        '
        'MaterialContextMenuStrip1
        '
        Me.MaterialContextMenuStrip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MaterialContextMenuStrip1.Depth = 0
        Me.MaterialContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RefreshToolStripMenuItem, Me.ToolStripSeparator1, Me.AddPeerToolStripMenuItem, Me.RemovePeerToolStripMenuItem})
        Me.MaterialContextMenuStrip1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialContextMenuStrip1.Name = "MaterialContextMenuStrip1"
        Me.MaterialContextMenuStrip1.Size = New System.Drawing.Size(144, 76)
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(143, 22)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(140, 6)
        '
        'AddPeerToolStripMenuItem
        '
        Me.AddPeerToolStripMenuItem.Name = "AddPeerToolStripMenuItem"
        Me.AddPeerToolStripMenuItem.Size = New System.Drawing.Size(143, 22)
        Me.AddPeerToolStripMenuItem.Text = "Add Peer"
        '
        'RemovePeerToolStripMenuItem
        '
        Me.RemovePeerToolStripMenuItem.Name = "RemovePeerToolStripMenuItem"
        Me.RemovePeerToolStripMenuItem.Size = New System.Drawing.Size(143, 22)
        Me.RemovePeerToolStripMenuItem.Text = "Remove Peer"
        '
        'MaterialRaisedButton2
        '
        Me.MaterialRaisedButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MaterialRaisedButton2.Depth = 0
        Me.MaterialRaisedButton2.Location = New System.Drawing.Point(12, 435)
        Me.MaterialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton2.Name = "MaterialRaisedButton2"
        Me.MaterialRaisedButton2.Primary = True
        Me.MaterialRaisedButton2.Size = New System.Drawing.Size(72, 29)
        Me.MaterialRaisedButton2.TabIndex = 8
        Me.MaterialRaisedButton2.Text = "Inspect"
        Me.MaterialRaisedButton2.UseVisualStyleBackColor = True
        '
        'l_myip
        '
        Me.l_myip.AutoSize = True
        Me.l_myip.Depth = 0
        Me.l_myip.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.l_myip.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.l_myip.Location = New System.Drawing.Point(12, 103)
        Me.l_myip.MouseState = MaterialSkin.MouseState.HOVER
        Me.l_myip.Name = "l_myip"
        Me.l_myip.Size = New System.Drawing.Size(26, 19)
        Me.l_myip.TabIndex = 11
        Me.l_myip.Text = "IP:"
        '
        'settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(284, 476)
        Me.Controls.Add(Me.l_myip)
        Me.Controls.Add(Me.c_part)
        Me.Controls.Add(Me.MaterialRaisedButton2)
        Me.Controls.Add(Me.dg)
        Me.Controls.Add(Me.c_ne)
        Me.Controls.Add(Me.MaterialDivider1)
        Me.Controls.Add(Me.MaterialFlatButton3)
        Me.Controls.Add(Me.MaterialRaisedButton1)
        Me.Controls.Add(Me.MaterialFlatButton1)
        Me.Controls.Add(Me.t_file)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "settings"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Settings"
        CType(Me.dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MaterialContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents t_file As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents MaterialFlatButton1 As MaterialSkin.Controls.MaterialFlatButton
    Friend WithEvents MaterialRaisedButton1 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents MaterialFlatButton3 As MaterialSkin.Controls.MaterialFlatButton
    Friend WithEvents MaterialDivider1 As MaterialSkin.Controls.MaterialDivider
    Friend WithEvents c_ne As MaterialSkin.Controls.MaterialCheckBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents dg As DataGridView
    Friend WithEvents pip As DataGridViewTextBoxColumn
    Friend WithEvents stats As DataGridViewTextBoxColumn
    Friend WithEvents MaterialRaisedButton2 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents MaterialContextMenuStrip1 As MaterialSkin.Controls.MaterialContextMenuStrip
    Friend WithEvents RefreshToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents AddPeerToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RemovePeerToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents c_part As MaterialSkin.Controls.MaterialCheckBox
    Friend WithEvents l_myip As MaterialSkin.Controls.MaterialLabel
End Class
