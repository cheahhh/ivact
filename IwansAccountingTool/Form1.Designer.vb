﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.stats = New System.Windows.Forms.StatusStrip()
        Me.l_pro = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Menu = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.stsm = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.RestartStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JournalsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsae = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LedgerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrialBalanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IncomeStatementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BalanceSheetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FullToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PartialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ManageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalculatorsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportToExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_mac = New System.Windows.Forms.ToolStripMenuItem()
        Me.tscm = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_sm = New System.Windows.Forms.ToolStripSeparator()
        Me.html_exp = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ConsoleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ADMS = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mtb = New MaterialSkin.Controls.MaterialTabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.field = New System.Windows.Forms.Panel()
        Me.gj_p = New System.Windows.Forms.Panel()
        Me.MaterialRaisedButton1 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.gj_q = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.l_field = New System.Windows.Forms.Panel()
        Me.p_ledger = New System.Windows.Forms.Panel()
        Me.t_q = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.tbal_dg = New System.Windows.Forms.DataGridView()
        Me.accs_d = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.amt_d = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Credit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tbal_p = New System.Windows.Forms.Panel()
        Me.tb_date = New MaterialSkin.Controls.MaterialLabel()
        Me.tb_name = New MaterialSkin.Controls.MaterialLabel()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.is_dg = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.col3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.is_p = New System.Windows.Forms.Panel()
        Me.is_date = New MaterialSkin.Controls.MaterialLabel()
        Me.is_name = New MaterialSkin.Controls.MaterialLabel()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.dg = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bs_p = New System.Windows.Forms.Panel()
        Me.bs_date = New MaterialSkin.Controls.MaterialLabel()
        Me.bs_name = New MaterialSkin.Controls.MaterialLabel()
        Me.mts = New MaterialSkin.Controls.MaterialTabSelector()
        Me.t_netw = New System.Windows.Forms.Timer(Me.components)
        Me.stats.SuspendLayout()
        Me.Menu.SuspendLayout()
        Me.mtb.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.gj_p.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.p_ledger.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        CType(Me.tbal_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbal_p.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.is_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.is_p.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        CType(Me.dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bs_p.SuspendLayout()
        Me.SuspendLayout()
        '
        'stats
        '
        Me.stats.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.l_pro})
        Me.stats.Location = New System.Drawing.Point(0, 448)
        Me.stats.Name = "stats"
        Me.stats.Size = New System.Drawing.Size(731, 22)
        Me.stats.TabIndex = 0
        Me.stats.Text = "StatusStrip1"
        '
        'l_pro
        '
        Me.l_pro.Name = "l_pro"
        Me.l_pro.Size = New System.Drawing.Size(76, 17)
        Me.l_pro.Text = "New Project*"
        '
        'Menu
        '
        Me.Menu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.JournalsToolStripMenuItem, Me.ToolStripMenuItem2, Me.ToolsToolStripMenuItem, Me.ADMS, Me.HelpToolStripMenuItem})
        Me.Menu.Location = New System.Drawing.Point(0, 0)
        Me.Menu.Name = "Menu"
        Me.Menu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.Menu.Size = New System.Drawing.Size(731, 24)
        Me.Menu.TabIndex = 1
        Me.Menu.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem4, Me.OpenToolStripMenuItem, Me.ToolStripMenuItem6, Me.stsm, Me.ToolStripSeparator2, Me.ToolStripMenuItem3, Me.ToolStripSeparator1, Me.RestartStripMenuItem1, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(186, 22)
        Me.ToolStripMenuItem4.Text = "New"
        '
        'OpenToolStripMenuItem
        '
        Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
        Me.OpenToolStripMenuItem.ShortcutKeyDisplayString = ""
        Me.OpenToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.OpenToolStripMenuItem.Text = "Open"
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        Me.ToolStripMenuItem6.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.ToolStripMenuItem6.Size = New System.Drawing.Size(186, 22)
        Me.ToolStripMenuItem6.Text = "Save"
        '
        'stsm
        '
        Me.stsm.Name = "stsm"
        Me.stsm.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.stsm.Size = New System.Drawing.Size(186, 22)
        Me.stsm.Text = "Save As"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(183, 6)
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(186, 22)
        Me.ToolStripMenuItem3.Text = "Close to File"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(183, 6)
        '
        'RestartStripMenuItem1
        '
        Me.RestartStripMenuItem1.Name = "RestartStripMenuItem1"
        Me.RestartStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.R), System.Windows.Forms.Keys)
        Me.RestartStripMenuItem1.Size = New System.Drawing.Size(186, 22)
        Me.RestartStripMenuItem1.Text = "Restart"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'JournalsToolStripMenuItem
        '
        Me.JournalsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsae, Me.ExportToolStripMenuItem})
        Me.JournalsToolStripMenuItem.Name = "JournalsToolStripMenuItem"
        Me.JournalsToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.JournalsToolStripMenuItem.Text = "Journals"
        '
        'tsae
        '
        Me.tsae.Name = "tsae"
        Me.tsae.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.tsae.Size = New System.Drawing.Size(168, 22)
        Me.tsae.Text = "Add entry"
        '
        'ExportToolStripMenuItem
        '
        Me.ExportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LedgerToolStripMenuItem, Me.TrialBalanceToolStripMenuItem, Me.IncomeStatementToolStripMenuItem, Me.BalanceSheetToolStripMenuItem})
        Me.ExportToolStripMenuItem.Name = "ExportToolStripMenuItem"
        Me.ExportToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ExportToolStripMenuItem.Text = "Post to"
        '
        'LedgerToolStripMenuItem
        '
        Me.LedgerToolStripMenuItem.Name = "LedgerToolStripMenuItem"
        Me.LedgerToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.L), System.Windows.Forms.Keys)
        Me.LedgerToolStripMenuItem.Size = New System.Drawing.Size(208, 22)
        Me.LedgerToolStripMenuItem.Text = "3 Column Ledger"
        '
        'TrialBalanceToolStripMenuItem
        '
        Me.TrialBalanceToolStripMenuItem.Name = "TrialBalanceToolStripMenuItem"
        Me.TrialBalanceToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.T), System.Windows.Forms.Keys)
        Me.TrialBalanceToolStripMenuItem.Size = New System.Drawing.Size(208, 22)
        Me.TrialBalanceToolStripMenuItem.Text = "Trial Balance"
        '
        'IncomeStatementToolStripMenuItem
        '
        Me.IncomeStatementToolStripMenuItem.Name = "IncomeStatementToolStripMenuItem"
        Me.IncomeStatementToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.IncomeStatementToolStripMenuItem.Size = New System.Drawing.Size(208, 22)
        Me.IncomeStatementToolStripMenuItem.Text = "Income Statement"
        '
        'BalanceSheetToolStripMenuItem
        '
        Me.BalanceSheetToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FullToolStripMenuItem, Me.PartialToolStripMenuItem})
        Me.BalanceSheetToolStripMenuItem.Name = "BalanceSheetToolStripMenuItem"
        Me.BalanceSheetToolStripMenuItem.Size = New System.Drawing.Size(208, 22)
        Me.BalanceSheetToolStripMenuItem.Text = "Balance Sheet"
        '
        'FullToolStripMenuItem
        '
        Me.FullToolStripMenuItem.Name = "FullToolStripMenuItem"
        Me.FullToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me.FullToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.FullToolStripMenuItem.Text = "Full"
        '
        'PartialToolStripMenuItem
        '
        Me.PartialToolStripMenuItem.Name = "PartialToolStripMenuItem"
        Me.PartialToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me.PartialToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.PartialToolStripMenuItem.Text = "Partial"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ManageToolStripMenuItem})
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(69, 20)
        Me.ToolStripMenuItem2.Text = "Accounts"
        '
        'ManageToolStripMenuItem
        '
        Me.ManageToolStripMenuItem.Name = "ManageToolStripMenuItem"
        Me.ManageToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.M), System.Windows.Forms.Keys)
        Me.ManageToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.ManageToolStripMenuItem.Text = "Manage"
        '
        'ToolsToolStripMenuItem
        '
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem5, Me.CalculatorsToolStripMenuItem, Me.ExportToExcelToolStripMenuItem, Me.ts_mac, Me.html_exp, Me.ToolStripSeparator3, Me.ConsoleToolStripMenuItem, Me.SettingsToolStripMenuItem})
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        Me.ToolsToolStripMenuItem.Size = New System.Drawing.Size(47, 20)
        Me.ToolsToolStripMenuItem.Text = "Tools"
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(150, 22)
        Me.ToolStripMenuItem5.Text = "Metadata"
        '
        'CalculatorsToolStripMenuItem
        '
        Me.CalculatorsToolStripMenuItem.Name = "CalculatorsToolStripMenuItem"
        Me.CalculatorsToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.CalculatorsToolStripMenuItem.Text = "Calculator"
        '
        'ExportToExcelToolStripMenuItem
        '
        Me.ExportToExcelToolStripMenuItem.Name = "ExportToExcelToolStripMenuItem"
        Me.ExportToExcelToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.ExportToExcelToolStripMenuItem.Text = "Export to Excel"
        '
        'ts_mac
        '
        Me.ts_mac.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tscm, Me.ts_sm})
        Me.ts_mac.Name = "ts_mac"
        Me.ts_mac.Size = New System.Drawing.Size(150, 22)
        Me.ts_mac.Text = "Macro"
        '
        'tscm
        '
        Me.tscm.Name = "tscm"
        Me.tscm.Size = New System.Drawing.Size(159, 22)
        Me.tscm.Text = "Manage Macros"
        '
        'ts_sm
        '
        Me.ts_sm.Name = "ts_sm"
        Me.ts_sm.Size = New System.Drawing.Size(156, 6)
        Me.ts_sm.Visible = False
        '
        'html_exp
        '
        Me.html_exp.Name = "html_exp"
        Me.html_exp.Size = New System.Drawing.Size(150, 22)
        Me.html_exp.Text = "Web Report"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(147, 6)
        '
        'ConsoleToolStripMenuItem
        '
        Me.ConsoleToolStripMenuItem.Name = "ConsoleToolStripMenuItem"
        Me.ConsoleToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.ConsoleToolStripMenuItem.Text = "Console"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'ADMS
        '
        Me.ADMS.Name = "ADMS"
        Me.ADMS.Size = New System.Drawing.Size(60, 20)
        Me.ADMS.Text = "Addons"
        Me.ADMS.Visible = False
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DocumentationToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'DocumentationToolStripMenuItem
        '
        Me.DocumentationToolStripMenuItem.Name = "DocumentationToolStripMenuItem"
        Me.DocumentationToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1
        Me.DocumentationToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.DocumentationToolStripMenuItem.Text = "Documentation"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'mtb
        '
        Me.mtb.Controls.Add(Me.TabPage2)
        Me.mtb.Controls.Add(Me.TabPage3)
        Me.mtb.Controls.Add(Me.TabPage4)
        Me.mtb.Controls.Add(Me.TabPage1)
        Me.mtb.Controls.Add(Me.TabPage5)
        Me.mtb.Depth = 0
        Me.mtb.Dock = System.Windows.Forms.DockStyle.Fill
        Me.mtb.Location = New System.Drawing.Point(0, 58)
        Me.mtb.MouseState = MaterialSkin.MouseState.HOVER
        Me.mtb.Name = "mtb"
        Me.mtb.SelectedIndex = 0
        Me.mtb.Size = New System.Drawing.Size(731, 390)
        Me.mtb.TabIndex = 3
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.field)
        Me.TabPage2.Controls.Add(Me.gj_p)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(723, 364)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "General Journal"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'field
        '
        Me.field.AllowDrop = True
        Me.field.AutoScroll = True
        Me.field.Dock = System.Windows.Forms.DockStyle.Fill
        Me.field.Location = New System.Drawing.Point(3, 35)
        Me.field.Name = "field"
        Me.field.Size = New System.Drawing.Size(717, 326)
        Me.field.TabIndex = 5
        '
        'gj_p
        '
        Me.gj_p.BackColor = System.Drawing.Color.White
        Me.gj_p.Controls.Add(Me.MaterialRaisedButton1)
        Me.gj_p.Controls.Add(Me.gj_q)
        Me.gj_p.Dock = System.Windows.Forms.DockStyle.Top
        Me.gj_p.Location = New System.Drawing.Point(3, 3)
        Me.gj_p.Name = "gj_p"
        Me.gj_p.Size = New System.Drawing.Size(717, 32)
        Me.gj_p.TabIndex = 1
        '
        'MaterialRaisedButton1
        '
        Me.MaterialRaisedButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MaterialRaisedButton1.Depth = 0
        Me.MaterialRaisedButton1.Location = New System.Drawing.Point(559, 0)
        Me.MaterialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton1.Name = "MaterialRaisedButton1"
        Me.MaterialRaisedButton1.Primary = True
        Me.MaterialRaisedButton1.Size = New System.Drawing.Size(158, 29)
        Me.MaterialRaisedButton1.TabIndex = 2
        Me.MaterialRaisedButton1.Text = "Add Journal Entry"
        Me.MaterialRaisedButton1.UseVisualStyleBackColor = True
        '
        'gj_q
        '
        Me.gj_q.Depth = 0
        Me.gj_q.Hint = "Search Journal Entry"
        Me.gj_q.Location = New System.Drawing.Point(3, 6)
        Me.gj_q.MaxLength = 32767
        Me.gj_q.MouseState = MaterialSkin.MouseState.HOVER
        Me.gj_q.Name = "gj_q"
        Me.gj_q.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.gj_q.SelectedText = ""
        Me.gj_q.SelectionLength = 0
        Me.gj_q.SelectionStart = 0
        Me.gj_q.Size = New System.Drawing.Size(306, 23)
        Me.gj_q.TabIndex = 1
        Me.gj_q.TabStop = False
        Me.gj_q.UseSystemPasswordChar = False
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.l_field)
        Me.TabPage3.Controls.Add(Me.p_ledger)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(723, 364)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Ledger"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'l_field
        '
        Me.l_field.AllowDrop = True
        Me.l_field.AutoScroll = True
        Me.l_field.Dock = System.Windows.Forms.DockStyle.Fill
        Me.l_field.Location = New System.Drawing.Point(3, 35)
        Me.l_field.Name = "l_field"
        Me.l_field.Size = New System.Drawing.Size(717, 326)
        Me.l_field.TabIndex = 4
        '
        'p_ledger
        '
        Me.p_ledger.BackColor = System.Drawing.Color.White
        Me.p_ledger.Controls.Add(Me.t_q)
        Me.p_ledger.Dock = System.Windows.Forms.DockStyle.Top
        Me.p_ledger.Location = New System.Drawing.Point(3, 3)
        Me.p_ledger.Name = "p_ledger"
        Me.p_ledger.Size = New System.Drawing.Size(717, 32)
        Me.p_ledger.TabIndex = 0
        '
        't_q
        '
        Me.t_q.Depth = 0
        Me.t_q.Hint = "Search Account Name"
        Me.t_q.Location = New System.Drawing.Point(3, 6)
        Me.t_q.MaxLength = 32767
        Me.t_q.MouseState = MaterialSkin.MouseState.HOVER
        Me.t_q.Name = "t_q"
        Me.t_q.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.t_q.SelectedText = ""
        Me.t_q.SelectionLength = 0
        Me.t_q.SelectionStart = 0
        Me.t_q.Size = New System.Drawing.Size(306, 23)
        Me.t_q.TabIndex = 1
        Me.t_q.TabStop = False
        Me.t_q.UseSystemPasswordChar = False
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.tbal_dg)
        Me.TabPage4.Controls.Add(Me.tbal_p)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(723, 364)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Trial Balance"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'tbal_dg
        '
        Me.tbal_dg.AllowUserToAddRows = False
        Me.tbal_dg.AllowUserToDeleteRows = False
        Me.tbal_dg.AllowUserToResizeColumns = False
        Me.tbal_dg.AllowUserToResizeRows = False
        Me.tbal_dg.BackgroundColor = System.Drawing.Color.White
        Me.tbal_dg.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tbal_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.tbal_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.accs_d, Me.amt_d, Me.Credit})
        Me.tbal_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbal_dg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.tbal_dg.GridColor = System.Drawing.Color.White
        Me.tbal_dg.Location = New System.Drawing.Point(3, 73)
        Me.tbal_dg.Name = "tbal_dg"
        Me.tbal_dg.ReadOnly = True
        Me.tbal_dg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.tbal_dg.Size = New System.Drawing.Size(717, 288)
        Me.tbal_dg.TabIndex = 10
        '
        'accs_d
        '
        Me.accs_d.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.accs_d.HeaderText = "Account"
        Me.accs_d.Name = "accs_d"
        Me.accs_d.ReadOnly = True
        '
        'amt_d
        '
        Me.amt_d.HeaderText = "Debit"
        Me.amt_d.Name = "amt_d"
        Me.amt_d.ReadOnly = True
        '
        'Credit
        '
        Me.Credit.HeaderText = "Credit"
        Me.Credit.Name = "Credit"
        Me.Credit.ReadOnly = True
        '
        'tbal_p
        '
        Me.tbal_p.BackColor = System.Drawing.Color.White
        Me.tbal_p.Controls.Add(Me.tb_date)
        Me.tbal_p.Controls.Add(Me.tb_name)
        Me.tbal_p.Dock = System.Windows.Forms.DockStyle.Top
        Me.tbal_p.Location = New System.Drawing.Point(3, 3)
        Me.tbal_p.Name = "tbal_p"
        Me.tbal_p.Size = New System.Drawing.Size(717, 70)
        Me.tbal_p.TabIndex = 0
        '
        'tb_date
        '
        Me.tb_date.AutoSize = True
        Me.tb_date.Depth = 0
        Me.tb_date.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.tb_date.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.tb_date.Location = New System.Drawing.Point(240, 34)
        Me.tb_date.MouseState = MaterialSkin.MouseState.HOVER
        Me.tb_date.Name = "tb_date"
        Me.tb_date.Size = New System.Drawing.Size(114, 19)
        Me.tb_date.TabIndex = 3
        Me.tb_date.Text = "Business Name"
        '
        'tb_name
        '
        Me.tb_name.AutoSize = True
        Me.tb_name.Depth = 0
        Me.tb_name.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.tb_name.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.tb_name.Location = New System.Drawing.Point(298, 17)
        Me.tb_name.MouseState = MaterialSkin.MouseState.HOVER
        Me.tb_name.Name = "tb_name"
        Me.tb_name.Size = New System.Drawing.Size(112, 19)
        Me.tb_name.TabIndex = 2
        Me.tb_name.Text = "Document Title"
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.is_dg)
        Me.TabPage1.Controls.Add(Me.is_p)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(723, 364)
        Me.TabPage1.TabIndex = 4
        Me.TabPage1.Text = "Income Statement"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'is_dg
        '
        Me.is_dg.AllowUserToAddRows = False
        Me.is_dg.AllowUserToDeleteRows = False
        Me.is_dg.AllowUserToResizeColumns = False
        Me.is_dg.AllowUserToResizeRows = False
        Me.is_dg.BackgroundColor = System.Drawing.Color.White
        Me.is_dg.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.is_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.is_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.col3})
        Me.is_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.is_dg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.is_dg.GridColor = System.Drawing.Color.White
        Me.is_dg.Location = New System.Drawing.Point(3, 73)
        Me.is_dg.Name = "is_dg"
        Me.is_dg.ReadOnly = True
        Me.is_dg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.is_dg.Size = New System.Drawing.Size(717, 288)
        Me.is_dg.TabIndex = 11
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = ""
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = ""
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'col3
        '
        Me.col3.HeaderText = ""
        Me.col3.Name = "col3"
        Me.col3.ReadOnly = True
        '
        'is_p
        '
        Me.is_p.BackColor = System.Drawing.Color.White
        Me.is_p.Controls.Add(Me.is_date)
        Me.is_p.Controls.Add(Me.is_name)
        Me.is_p.Dock = System.Windows.Forms.DockStyle.Top
        Me.is_p.Location = New System.Drawing.Point(3, 3)
        Me.is_p.Name = "is_p"
        Me.is_p.Size = New System.Drawing.Size(717, 70)
        Me.is_p.TabIndex = 1
        '
        'is_date
        '
        Me.is_date.AutoSize = True
        Me.is_date.Depth = 0
        Me.is_date.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.is_date.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.is_date.Location = New System.Drawing.Point(240, 34)
        Me.is_date.MouseState = MaterialSkin.MouseState.HOVER
        Me.is_date.Name = "is_date"
        Me.is_date.Size = New System.Drawing.Size(114, 19)
        Me.is_date.TabIndex = 3
        Me.is_date.Text = "Business Name"
        '
        'is_name
        '
        Me.is_name.AutoSize = True
        Me.is_name.Depth = 0
        Me.is_name.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.is_name.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.is_name.Location = New System.Drawing.Point(298, 17)
        Me.is_name.MouseState = MaterialSkin.MouseState.HOVER
        Me.is_name.Name = "is_name"
        Me.is_name.Size = New System.Drawing.Size(112, 19)
        Me.is_name.TabIndex = 2
        Me.is_name.Text = "Document Title"
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.dg)
        Me.TabPage5.Controls.Add(Me.bs_p)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(723, 364)
        Me.TabPage5.TabIndex = 5
        Me.TabPage5.Text = "Balance Sheet"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'dg
        '
        Me.dg.AllowUserToAddRows = False
        Me.dg.AllowUserToDeleteRows = False
        Me.dg.AllowUserToResizeColumns = False
        Me.dg.AllowUserToResizeRows = False
        Me.dg.BackgroundColor = System.Drawing.Color.White
        Me.dg.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7})
        Me.dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dg.GridColor = System.Drawing.Color.White
        Me.dg.Location = New System.Drawing.Point(3, 73)
        Me.dg.Name = "dg"
        Me.dg.ReadOnly = True
        Me.dg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg.Size = New System.Drawing.Size(717, 288)
        Me.dg.TabIndex = 14
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.HeaderText = ""
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = ""
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = ""
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = ""
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'bs_p
        '
        Me.bs_p.BackColor = System.Drawing.Color.White
        Me.bs_p.Controls.Add(Me.bs_date)
        Me.bs_p.Controls.Add(Me.bs_name)
        Me.bs_p.Dock = System.Windows.Forms.DockStyle.Top
        Me.bs_p.Location = New System.Drawing.Point(3, 3)
        Me.bs_p.Name = "bs_p"
        Me.bs_p.Size = New System.Drawing.Size(717, 70)
        Me.bs_p.TabIndex = 2
        '
        'bs_date
        '
        Me.bs_date.AutoSize = True
        Me.bs_date.Depth = 0
        Me.bs_date.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.bs_date.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.bs_date.Location = New System.Drawing.Point(240, 34)
        Me.bs_date.MouseState = MaterialSkin.MouseState.HOVER
        Me.bs_date.Name = "bs_date"
        Me.bs_date.Size = New System.Drawing.Size(114, 19)
        Me.bs_date.TabIndex = 3
        Me.bs_date.Text = "Business Name"
        '
        'bs_name
        '
        Me.bs_name.AutoSize = True
        Me.bs_name.Depth = 0
        Me.bs_name.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.bs_name.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.bs_name.Location = New System.Drawing.Point(298, 17)
        Me.bs_name.MouseState = MaterialSkin.MouseState.HOVER
        Me.bs_name.Name = "bs_name"
        Me.bs_name.Size = New System.Drawing.Size(112, 19)
        Me.bs_name.TabIndex = 2
        Me.bs_name.Text = "Document Title"
        '
        'mts
        '
        Me.mts.BaseTabControl = Me.mtb
        Me.mts.Depth = 0
        Me.mts.Dock = System.Windows.Forms.DockStyle.Top
        Me.mts.Location = New System.Drawing.Point(0, 24)
        Me.mts.MouseState = MaterialSkin.MouseState.HOVER
        Me.mts.Name = "mts"
        Me.mts.Size = New System.Drawing.Size(731, 34)
        Me.mts.TabIndex = 2
        Me.mts.Text = "MaterialTabSelector1"
        '
        't_netw
        '
        '
        'MainF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(731, 470)
        Me.Controls.Add(Me.mtb)
        Me.Controls.Add(Me.mts)
        Me.Controls.Add(Me.stats)
        Me.Controls.Add(Me.Menu)
        Me.MainMenuStrip = Me.Menu
        Me.Name = "MainF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ivan's Accounting Tools - General Journal"
        Me.stats.ResumeLayout(False)
        Me.stats.PerformLayout()
        Me.Menu.ResumeLayout(False)
        Me.Menu.PerformLayout()
        Me.mtb.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.gj_p.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.p_ledger.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        CType(Me.tbal_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbal_p.ResumeLayout(False)
        Me.tbal_p.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        CType(Me.is_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.is_p.ResumeLayout(False)
        Me.is_p.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        CType(Me.dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bs_p.ResumeLayout(False)
        Me.bs_p.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents stats As StatusStrip
    Friend WithEvents Menu As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents JournalsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents tsae As ToolStripMenuItem
    Friend WithEvents ExportToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LedgerToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TrialBalanceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents IncomeStatementToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BalanceSheetToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CalculatorsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OpenToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents stsm As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents ManageToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FullToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PartialToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DocumentationToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ADMS As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ToolStripMenuItem3 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As ToolStripMenuItem
    Friend WithEvents l_pro As ToolStripStatusLabel
    Friend WithEvents mtb As MaterialSkin.Controls.MaterialTabControl
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents mts As MaterialSkin.Controls.MaterialTabSelector
    Friend WithEvents p_ledger As Panel
    Friend WithEvents t_q As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents l_field As Panel
    Friend WithEvents tbal_dg As DataGridView
    Friend WithEvents accs_d As DataGridViewTextBoxColumn
    Friend WithEvents amt_d As DataGridViewTextBoxColumn
    Friend WithEvents Credit As DataGridViewTextBoxColumn
    Friend WithEvents tbal_p As Panel
    Friend WithEvents tb_date As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents tb_name As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents is_p As Panel
    Friend WithEvents is_date As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents is_name As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents is_dg As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents col3 As DataGridViewTextBoxColumn
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents bs_p As Panel
    Friend WithEvents bs_date As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents bs_name As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents dg As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents ToolStripMenuItem5 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem6 As ToolStripMenuItem
    Friend WithEvents field As Panel
    Friend WithEvents gj_p As Panel
    Friend WithEvents gj_q As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents MaterialRaisedButton1 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents ExportToExcelToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents ConsoleToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents html_exp As ToolStripMenuItem
    Friend WithEvents ts_mac As ToolStripMenuItem
    Friend WithEvents tscm As ToolStripMenuItem
    Friend WithEvents ts_sm As ToolStripSeparator
    Friend WithEvents SettingsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RestartStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents t_netw As Timer
End Class
