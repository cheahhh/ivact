﻿Imports System.ComponentModel
Imports System.Windows.Forms.DataVisualization.Charting
Public Class tricolledger
    Sub selfbal()
        dg.Sort(dg.Columns(2), ListSortDirection.Ascending)
        For i = 0 To dg.Rows.Count - 1
            If Not dg.Rows(i).Cells(3).Value = "" Then
                If i = 0 Then
                    If MainF.rtype(LName.Text) = 1 Then
                        dg.Rows(i).Cells(5).Value = Decimal.Parse(dg.Rows(i).Cells(3).Value * -1)
                    Else
                        dg.Rows(i).Cells(5).Value = Decimal.Parse(dg.Rows(i).Cells(3).Value)
                    End If
                Else
                    If MainF.rtype(LName.Text) = 1 Then
                        dg.Rows(i).Cells(5).Value = Decimal.Parse(dg.Rows(i - 1).Cells(5).Value) - Decimal.Parse(dg.Rows(i).Cells(3).Value)
                    Else
                        dg.Rows(i).Cells(5).Value = Decimal.Parse(dg.Rows(i - 1).Cells(5).Value) + Decimal.Parse(dg.Rows(i).Cells(3).Value)
                    End If
                End If
            Else
                If i = 0 Then
                    If MainF.rtype(LName.Text) = 1 Then
                        dg.Rows(i).Cells(5).Value = Decimal.Parse(dg.Rows(i).Cells(4).Value)
                    Else
                        dg.Rows(i).Cells(5).Value = Decimal.Parse((dg.Rows(i).Cells(4).Value * -1))
                    End If
                Else
                    If MainF.rtype(LName.Text) = 1 Then
                        dg.Rows(i).Cells(5).Value = Decimal.Parse(dg.Rows(i - 1).Cells(5).Value) + Decimal.Parse(dg.Rows(i).Cells(4).Value)
                    Else
                        dg.Rows(i).Cells(5).Value = Decimal.Parse(dg.Rows(i - 1).Cells(5).Value) - Decimal.Parse(dg.Rows(i).Cells(4).Value)
                    End If
                End If
            End If
        Next
    End Sub
    Function dumpcode()
        Dim az As String
        az = az & vbNewLine & "<AC>" & LName.Text
        For i = 0 To dg.Rows.Count - 1
            az = az & vbNewLine & "<LI>" & dg.Rows(i).Cells(0).Value & "/" & dg.Rows(i).Cells(1).Value & "/" & dg.Rows(i).Cells(2).Value & "/" & dg.Rows(i).Cells(3).Value & "/" & dg.Rows(i).Cells(4).Value & "/" & dg.Rows(i).Cells(5).Value
        Next
        az = az & vbNewLine & "</AC>"
        Return az
    End Function

    Private Sub LName_DoubleClick(sender As Object, e As EventArgs) Handles LName.DoubleClick
        charter.Show()
        charter.ch.Series.Clear()
        charter.ch.Titles.Clear()
        charter.ch.Titles.Add("Cash Flow for " & LName.Text)
        charter.Text = "Cash Flow for " & LName.Text
        Dim z As New Series
        z.ChartType = SeriesChartType.Line
        z.Name = "Balance"
        For i = 0 To dg.Rows.Count - 1
            z.Points.AddXY(dg.Rows(i).Cells(0).Value, dg.Rows(i).Cells(5).Value)
        Next
        charter.ch.Series.Add(z)
    End Sub
End Class
