﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class medit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.bname = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.stdate = New MaterialSkin.Controls.MaterialRadioButton()
        Me.cusdate = New MaterialSkin.Controls.MaterialRadioButton()
        Me.ml1 = New MaterialSkin.Controls.MaterialLabel()
        Me.ml2 = New MaterialSkin.Controls.MaterialLabel()
        Me.cudate = New System.Windows.Forms.DateTimePicker()
        Me.edate = New System.Windows.Forms.DateTimePicker()
        Me.MaterialRaisedButton1 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.SuspendLayout()
        '
        'bname
        '
        Me.bname.Depth = 0
        Me.bname.Hint = "Business Name"
        Me.bname.Location = New System.Drawing.Point(12, 12)
        Me.bname.MaxLength = 32767
        Me.bname.MouseState = MaterialSkin.MouseState.HOVER
        Me.bname.Name = "bname"
        Me.bname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.bname.SelectedText = ""
        Me.bname.SelectionLength = 0
        Me.bname.SelectionStart = 0
        Me.bname.Size = New System.Drawing.Size(203, 23)
        Me.bname.TabIndex = 0
        Me.bname.TabStop = False
        Me.bname.UseSystemPasswordChar = False
        '
        'stdate
        '
        Me.stdate.AutoSize = True
        Me.stdate.Checked = True
        Me.stdate.Depth = 0
        Me.stdate.Font = New System.Drawing.Font("Roboto", 10.0!)
        Me.stdate.Location = New System.Drawing.Point(9, 38)
        Me.stdate.Margin = New System.Windows.Forms.Padding(0)
        Me.stdate.MouseLocation = New System.Drawing.Point(-1, -1)
        Me.stdate.MouseState = MaterialSkin.MouseState.HOVER
        Me.stdate.Name = "stdate"
        Me.stdate.Ripple = True
        Me.stdate.Size = New System.Drawing.Size(246, 30)
        Me.stdate.TabIndex = 1
        Me.stdate.TabStop = True
        Me.stdate.Text = "Use standard dates for statements."
        Me.stdate.UseVisualStyleBackColor = True
        '
        'cusdate
        '
        Me.cusdate.AutoSize = True
        Me.cusdate.Depth = 0
        Me.cusdate.Font = New System.Drawing.Font("Roboto", 10.0!)
        Me.cusdate.Location = New System.Drawing.Point(9, 68)
        Me.cusdate.Margin = New System.Windows.Forms.Padding(0)
        Me.cusdate.MouseLocation = New System.Drawing.Point(-1, -1)
        Me.cusdate.MouseState = MaterialSkin.MouseState.HOVER
        Me.cusdate.Name = "cusdate"
        Me.cusdate.Ripple = True
        Me.cusdate.Size = New System.Drawing.Size(237, 30)
        Me.cusdate.TabIndex = 2
        Me.cusdate.Text = "Use custom dates for statements."
        Me.cusdate.UseVisualStyleBackColor = True
        '
        'ml1
        '
        Me.ml1.AutoSize = True
        Me.ml1.Depth = 0
        Me.ml1.Enabled = False
        Me.ml1.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.ml1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ml1.Location = New System.Drawing.Point(12, 98)
        Me.ml1.MouseState = MaterialSkin.MouseState.HOVER
        Me.ml1.Name = "ml1"
        Me.ml1.Size = New System.Drawing.Size(95, 19)
        Me.ml1.TabIndex = 3
        Me.ml1.Text = "Current date:"
        '
        'ml2
        '
        Me.ml2.AutoSize = True
        Me.ml2.Depth = 0
        Me.ml2.Enabled = False
        Me.ml2.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.ml2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ml2.Location = New System.Drawing.Point(12, 125)
        Me.ml2.MouseState = MaterialSkin.MouseState.HOVER
        Me.ml2.Name = "ml2"
        Me.ml2.Size = New System.Drawing.Size(97, 19)
        Me.ml2.TabIndex = 4
        Me.ml2.Text = "Closing date:"
        '
        'cudate
        '
        Me.cudate.Enabled = False
        Me.cudate.Location = New System.Drawing.Point(111, 97)
        Me.cudate.Name = "cudate"
        Me.cudate.Size = New System.Drawing.Size(200, 20)
        Me.cudate.TabIndex = 5
        '
        'edate
        '
        Me.edate.Enabled = False
        Me.edate.Location = New System.Drawing.Point(111, 124)
        Me.edate.Name = "edate"
        Me.edate.Size = New System.Drawing.Size(200, 20)
        Me.edate.TabIndex = 6
        '
        'MaterialRaisedButton1
        '
        Me.MaterialRaisedButton1.Depth = 0
        Me.MaterialRaisedButton1.Location = New System.Drawing.Point(236, 150)
        Me.MaterialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton1.Name = "MaterialRaisedButton1"
        Me.MaterialRaisedButton1.Primary = True
        Me.MaterialRaisedButton1.Size = New System.Drawing.Size(75, 23)
        Me.MaterialRaisedButton1.TabIndex = 7
        Me.MaterialRaisedButton1.Text = "Save"
        Me.MaterialRaisedButton1.UseVisualStyleBackColor = True
        '
        'medit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(323, 184)
        Me.Controls.Add(Me.MaterialRaisedButton1)
        Me.Controls.Add(Me.edate)
        Me.Controls.Add(Me.cudate)
        Me.Controls.Add(Me.ml2)
        Me.Controls.Add(Me.ml1)
        Me.Controls.Add(Me.cusdate)
        Me.Controls.Add(Me.stdate)
        Me.Controls.Add(Me.bname)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "medit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Metadata Editor"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents bname As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents stdate As MaterialSkin.Controls.MaterialRadioButton
    Friend WithEvents cusdate As MaterialSkin.Controls.MaterialRadioButton
    Friend WithEvents ml1 As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents ml2 As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents cudate As DateTimePicker
    Friend WithEvents edate As DateTimePicker
    Friend WithEvents MaterialRaisedButton1 As MaterialSkin.Controls.MaterialRaisedButton
End Class
