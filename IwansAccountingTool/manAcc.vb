﻿Public Class manAcc
    Private Sub manAcc_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For Each item As String In MainF.acclistdet.Cast(Of String)()
            Dim z As String() = item.Split("/")
            If z.Count = 3 Then
                dg.Rows.Add(New String() {z(0), z(1), z(2)})
            Else
                dg.Rows.Add(New String() {z(0), z(1)})
            End If
        Next
        For i As Integer = 0 To dg.RowCount - 1
            dg.Rows(i).ContextMenuStrip = cms
        Next
    End Sub

    Private Sub EditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EditToolStripMenuItem.Click
        addacc.Show()
        addacc.mode = 1
        addacc.mint = dg.CurrentCell.RowIndex
        addacc.Text = "Edit Account"
        addacc.Button1.Text = "Edit Account"
        addacc.tname.Text = dg.Rows(dg.CurrentCell.RowIndex).Cells(0).Value
        addacc.cbt.Text = dg.Rows(dg.CurrentCell.RowIndex).Cells(1).Value
        addacc.tbal.Text = dg.Rows(dg.CurrentCell.RowIndex).Cells(2).Value
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteToolStripMenuItem.Click
        If MsgBox("Delete?", MsgBoxStyle.OkCancel, "Are you sure?") = MsgBoxResult.Ok Then
            dg.Rows.RemoveAt(dg.CurrentCell.RowIndex)
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If MsgBox("Proceed?", MsgBoxStyle.OkCancel, "Changes cannot be reverted.") = MsgBoxResult.Ok Then
            MainF.acclist.Clear()
            MainF.acclistdet.Clear()
            For i As Integer = 0 To dg.RowCount - 1
                MainF.acclist.Add(dg.Rows(i).Cells(0).Value)
                MainF.acclistdet.Add(dg.Rows(i).Cells(0).Value & "/" & dg.Rows(i).Cells(1).Value & "/" & dg.Rows(i).Cells(2).Value)
            Next
            Me.Close()
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        addacc.Show()
        addacc.mode = 2
        addacc.k_open.Visible = True
    End Sub
End Class