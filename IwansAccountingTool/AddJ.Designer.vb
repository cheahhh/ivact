﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AddJ
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtp = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dg_d = New System.Windows.Forms.DataGridView()
        Me.accs_d = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.amt_d = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.t_amt_d = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbd = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.dg_c = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.t_amt_c = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbc = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.t_desc = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.l_j = New System.Windows.Forms.Label()
        Me.k_open = New System.Windows.Forms.CheckBox()
        Me.cms_edit = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SenderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dg_d, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dg_c, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cms_edit.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Date:"
        '
        'dtp
        '
        Me.dtp.CustomFormat = "dd MMMM yyyy"
        Me.dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp.Location = New System.Drawing.Point(51, 9)
        Me.dtp.Name = "dtp"
        Me.dtp.Size = New System.Drawing.Size(153, 20)
        Me.dtp.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.dg_d)
        Me.GroupBox1.Controls.Add(Me.t_amt_d)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.cbd)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 35)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(272, 246)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Debit"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(195, 38)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(71, 23)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Add"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dg_d
        '
        Me.dg_d.AllowUserToAddRows = False
        Me.dg_d.AllowUserToResizeColumns = False
        Me.dg_d.AllowUserToResizeRows = False
        Me.dg_d.BackgroundColor = System.Drawing.Color.White
        Me.dg_d.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dg_d.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_d.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.accs_d, Me.amt_d})
        Me.dg_d.ContextMenuStrip = Me.cms_edit
        Me.dg_d.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dg_d.GridColor = System.Drawing.Color.White
        Me.dg_d.Location = New System.Drawing.Point(6, 67)
        Me.dg_d.Name = "dg_d"
        Me.dg_d.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_d.Size = New System.Drawing.Size(260, 173)
        Me.dg_d.TabIndex = 8
        '
        'accs_d
        '
        Me.accs_d.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.accs_d.HeaderText = "Account(s)"
        Me.accs_d.Name = "accs_d"
        '
        'amt_d
        '
        Me.amt_d.HeaderText = "Amount"
        Me.amt_d.Name = "amt_d"
        '
        't_amt_d
        '
        Me.t_amt_d.Location = New System.Drawing.Point(68, 40)
        Me.t_amt_d.Name = "t_amt_d"
        Me.t_amt_d.Size = New System.Drawing.Size(121, 20)
        Me.t_amt_d.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 43)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Amount:"
        '
        'cbd
        '
        Me.cbd.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbd.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbd.FormattingEnabled = True
        Me.cbd.Location = New System.Drawing.Point(68, 13)
        Me.cbd.Name = "cbd"
        Me.cbd.Size = New System.Drawing.Size(198, 21)
        Me.cbd.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Account(s):"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.dg_c)
        Me.GroupBox2.Controls.Add(Me.t_amt_c)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.cbc)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Location = New System.Drawing.Point(299, 35)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(272, 246)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Credit"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(195, 38)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(71, 23)
        Me.Button2.TabIndex = 9
        Me.Button2.Text = "Add"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'dg_c
        '
        Me.dg_c.AllowUserToAddRows = False
        Me.dg_c.AllowUserToResizeColumns = False
        Me.dg_c.AllowUserToResizeRows = False
        Me.dg_c.BackgroundColor = System.Drawing.Color.White
        Me.dg_c.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dg_c.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_c.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.dg_c.ContextMenuStrip = Me.cms_edit
        Me.dg_c.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dg_c.GridColor = System.Drawing.Color.White
        Me.dg_c.Location = New System.Drawing.Point(6, 67)
        Me.dg_c.Name = "dg_c"
        Me.dg_c.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_c.Size = New System.Drawing.Size(260, 173)
        Me.dg_c.TabIndex = 8
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Account(s)"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        't_amt_c
        '
        Me.t_amt_c.Location = New System.Drawing.Point(68, 40)
        Me.t_amt_c.Name = "t_amt_c"
        Me.t_amt_c.Size = New System.Drawing.Size(121, 20)
        Me.t_amt_c.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 43)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Amount:"
        '
        'cbc
        '
        Me.cbc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbc.FormattingEnabled = True
        Me.cbc.Location = New System.Drawing.Point(68, 13)
        Me.cbc.Name = "cbc"
        Me.cbc.Size = New System.Drawing.Size(198, 21)
        Me.cbc.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Account(s):"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 290)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 13)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Description:"
        '
        't_desc
        '
        Me.t_desc.Location = New System.Drawing.Point(81, 287)
        Me.t_desc.Name = "t_desc"
        Me.t_desc.Size = New System.Drawing.Size(490, 20)
        Me.t_desc.TabIndex = 10
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(411, 313)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(77, 23)
        Me.Button3.TabIndex = 10
        Me.Button3.Text = "Add Entry"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(494, 313)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(77, 23)
        Me.Button4.TabIndex = 11
        Me.Button4.Text = "Cancel"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'l_j
        '
        Me.l_j.AutoSize = True
        Me.l_j.Location = New System.Drawing.Point(427, 9)
        Me.l_j.Name = "l_j"
        Me.l_j.Size = New System.Drawing.Size(61, 13)
        Me.l_j.TabIndex = 12
        Me.l_j.Text = "Journal No:"
        '
        'k_open
        '
        Me.k_open.AutoSize = True
        Me.k_open.Location = New System.Drawing.Point(283, 317)
        Me.k_open.Name = "k_open"
        Me.k_open.Size = New System.Drawing.Size(122, 17)
        Me.k_open.TabIndex = 13
        Me.k_open.Text = "Keep Window Open"
        Me.k_open.UseVisualStyleBackColor = True
        '
        'cms_edit
        '
        Me.cms_edit.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SenderToolStripMenuItem})
        Me.cms_edit.Name = "cms_edit"
        Me.cms_edit.Size = New System.Drawing.Size(108, 26)
        '
        'SenderToolStripMenuItem
        '
        Me.SenderToolStripMenuItem.Name = "SenderToolStripMenuItem"
        Me.SenderToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.SenderToolStripMenuItem.Text = "Delete"
        '
        'AddJ
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(583, 341)
        Me.Controls.Add(Me.k_open)
        Me.Controls.Add(Me.l_j)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.t_desc)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dtp)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "AddJ"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add Journal Entry"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dg_d, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dg_c, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cms_edit.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label2 As Label
    Friend WithEvents dtp As DateTimePicker
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents t_amt_d As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents cbd As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents dg_d As DataGridView
    Friend WithEvents accs_d As DataGridViewTextBoxColumn
    Friend WithEvents amt_d As DataGridViewTextBoxColumn
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Button2 As Button
    Friend WithEvents dg_c As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents t_amt_c As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents cbc As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents t_desc As TextBox
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents l_j As Label
    Friend WithEvents k_open As CheckBox
    Friend WithEvents cms_edit As ContextMenuStrip
    Friend WithEvents SenderToolStripMenuItem As ToolStripMenuItem
End Class
