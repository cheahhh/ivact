﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TriColLedgerF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TriColLedgerF))
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.t_q = New System.Windows.Forms.ToolStripTextBox()
        Me.b_exp = New System.Windows.Forms.ToolStripButton()
        Me.field = New System.Windows.Forms.Panel()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 431)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(725, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.t_q, Me.b_exp})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(725, 25)
        Me.ToolStrip1.TabIndex = 2
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(45, 22)
        Me.ToolStripLabel1.Text = "Search:"
        '
        't_q
        '
        Me.t_q.BackColor = System.Drawing.SystemColors.Window
        Me.t_q.Name = "t_q"
        Me.t_q.Size = New System.Drawing.Size(100, 25)
        '
        'b_exp
        '
        Me.b_exp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.b_exp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.b_exp.Image = CType(resources.GetObject("b_exp.Image"), System.Drawing.Image)
        Me.b_exp.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.b_exp.Name = "b_exp"
        Me.b_exp.Size = New System.Drawing.Size(44, 22)
        Me.b_exp.Text = "Export"
        '
        'field
        '
        Me.field.AutoScroll = True
        Me.field.Dock = System.Windows.Forms.DockStyle.Fill
        Me.field.Location = New System.Drawing.Point(0, 25)
        Me.field.Name = "field"
        Me.field.Size = New System.Drawing.Size(725, 406)
        Me.field.TabIndex = 3
        '
        'TriColLedgerF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(725, 453)
        Me.Controls.Add(Me.field)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "TriColLedgerF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Three Column Ledger"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripLabel1 As ToolStripLabel
    Friend WithEvents t_q As ToolStripTextBox
    Friend WithEvents field As Panel
    Friend WithEvents b_exp As ToolStripButton
End Class
