﻿Public Class consolas
    Private Sub consolas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        log.Text = MainF.log
    End Sub
    Sub parse(ByVal az As String)
        If az.Contains("setw ") Then
            MainF.Width = az.Replace("setw ", "")
            psuc(az)
        End If
        If az.Contains("seth ") Then
            MainF.Height = az.Replace("seth ", "")
            psuc(az)
        End If
        If az.Contains("eexcel ") Then
            MainF.eexcel(az.Replace("eexcel ", ""))
            psuc(az)
        End If
        If az.Contains("openfile ") Then
            MainF.openfile(az.Replace("openfile ", ""))
            psuc(az)
        End If
        If az.Contains("returnopenbal ") Then
            psuc("Open Balance for " & az.Replace("returnopenbal ", "") & ": " & MainF.returnopenbal(az.Replace("returnopenbal ", "")))
        End If
        If az.Contains("returncbal ") Then
            psuc("Current Balance for " & az.Replace("returncbal ", "") & ": " & MainF.returncbal(az.Replace("returncbal ", "")))
        End If
        If az.Contains("setdefaultfile ") Then
            My.Settings.defaultfile = az.Replace("setdefaultfile ", "")
            My.Settings.Save()
            psuc("Default opening file set to: " & IO.Path.GetFileNameWithoutExtension(My.Settings.defaultfile))
        End If
        If az.Contains("itester") Then
            msg_sim.Show()
            psuc("Message Simulator")
        End If
    End Sub
    Sub psuc(ByVal za As String)
        MainF.wl(za)
        comd.Clear()
    End Sub
    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton1.Click
        parse(comd.Text)
    End Sub

    Private Sub comd_KeyPress(sender As Object, e As KeyPressEventArgs) Handles comd.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            MaterialRaisedButton1.PerformClick()
        End If
    End Sub
End Class