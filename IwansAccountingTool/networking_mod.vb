﻿Imports System.IO
Imports System.Net.Sockets

Module networking_mod
    Public Client As New TcpClient
    Public myip As String
    Public plog As String
    Public Sub sendmessage(ByVal ip As String, ByVal mesg As String)
        Try
            Client = New TcpClient(ip, 52347)
            Dim Writer As New StreamWriter(Client.GetStream())
            Writer.Write(mesg)
            Writer.Flush()
        Catch ex As Exception
            Console.WriteLine(ex)
            Dim Errorresult As String = ex.Message
            MessageBox.Show(Errorresult & vbCrLf & vbCrLf &
                            "Please Review Client Address",
                            "Error Sending Message",
                            MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Public Sub procmsg(ByVal mesg As String)
        If mesg.Contains("[ADDIP]") Then
            sendmessage(mesg.Split("]")(1), "[ADDIP]" & myip & "]" & settings.c_part.Checked)
            If mesg.Split("]")(2) = True Then settings.dg.Rows.Add(New String() {mesg.Split("]")(1), "Participating"}) Else settings.dg.Rows.Add(New String() {mesg.Split("]")(1), "Spectating"})
        ElseIf mesg.Contains("[REMIP]") Then
            For i = 0 To settings.dg.Rows.Count - 1
                If settings.dg.Rows(i).Cells(0).Value = mesg.Replace("[REMIP]", "") Then settings.dg.Rows.RemoveAt(i)
            Next
        ElseIf mesg.Contains("[PCNE]") Then
            Dim tarray As New ArrayList
            Dim az As String() = mesg.Split(vbCrLf)
            tarray.AddRange(az)
            tarray.RemoveAt(0)
            MainF.parsefile(tarray.ToArray)
            wlog("New Journal Entry" & vbNewLine & mesg.Replace("[PCNE]", ""))
        ElseIf mesg.Contains("[SCNE]") Then
            wlog("New Journal Entry Observed" & vbNewLine & mesg.Replace("[SCNE]", ""))
        ElseIf mesg.Contains("[PCEE]") Then
            Dim tarray As New ArrayList
            Dim az As String() = mesg.Split(vbCrLf)
            tarray.AddRange(az)
            tarray.RemoveAt(0)
            For Each cz As Control In MainF.field.Controls
                If cz.Name = tarray.Item(0).replace("<J>", "") Then
                    MainF.field.Controls.Remove(cz)
                End If
            Next
            MainF.parsefile(tarray.ToArray)
            wlog("Edit Existing Journal Entry" & vbNewLine & mesg.Replace("[PCEE]", ""))
        ElseIf mesg.Contains("[SCEE]") Then
            wlog("Journal Entry Edit Observed" & vbNewLine & mesg.Replace("[SCEE]", ""))
        ElseIf mesg.Contains("[PCAA]") Then
            Dim tarray As New ArrayList
            Dim az As String() = mesg.Split(vbCrLf)
            tarray.AddRange(az)
            tarray.RemoveAt(0)
            MainF.parsefile(tarray.ToArray)
            wlog("Add New Account" & vbNewLine & mesg.Replace("[PCAA]", ""))
        ElseIf mesg.Contains("[SCAA]") Then
            wlog("New Account Addition Observed" & vbNewLine & mesg.Replace("[SCAA]", ""))
        End If
    End Sub
    'Command definitions
    '[ADDIP] = Add IP and Status
    '[PCNE] = Participating, New Entry
    '[SCNE] = Spectating, New Entry
    '[PCEE] = Participating, Edit Entry
    '[SCEE] = Spectating, Edit Entry
    '[PCAA] = Participating, New Account
    '[SCAA] = Spectating, New Account
    Sub wlog(ByVal z As String)
        If plog = "" Then plog = z Else plog = plog & vbNewLine & z
    End Sub
    Sub updatenewentry(ByVal z As String)
        For i = 0 To settings.dg.Rows.Count - 1
            If settings.dg.Rows(i).Cells(1).Value = "Participating" Then sendmessage(settings.dg.Rows(i).Cells(0).Value, "[PCNE]" & vbNewLine & z) Else sendmessage(settings.dg.Rows(i).Cells(0).Value, "[SCNE]" & vbNewLine & z)
        Next
    End Sub
    Sub editoldentry(ByVal z As String)
        For i = 0 To settings.dg.Rows.Count - 1
            If settings.dg.Rows(i).Cells(1).Value = "Participating" Then sendmessage(settings.dg.Rows(i).Cells(0).Value, "[PCEE]" & vbNewLine & z) Else sendmessage(settings.dg.Rows(i).Cells(0).Value, "[SCEE]" & vbNewLine & z)
        Next
    End Sub
    Sub addnewaccount(ByVal z As String)
        For i = 0 To settings.dg.Rows.Count - 1
            If settings.dg.Rows(i).Cells(1).Value = "Participating" Then sendmessage(settings.dg.Rows(i).Cells(0).Value, "[PCAA]" & vbNewLine & z) Else sendmessage(settings.dg.Rows(i).Cells(0).Value, "[SCAA]" & vbNewLine & z)
        Next
    End Sub
    Sub net_removepeer(ByVal z As String)
        For i = 0 To settings.dg.Rows.Count - 1
            sendmessage(settings.dg.Rows(i).Cells(0).Value, "[REMIP]" & z)
        Next
    End Sub
End Module
