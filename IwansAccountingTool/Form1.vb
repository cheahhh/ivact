﻿Imports System.IO
Imports System.Net.Sockets
Imports System.Reflection

Public Class MainF
    Dim Message As String = ""
    Public Listener As New TcpListener(52347)
    Public filep As String
    Public acclist As New ArrayList
    Public acclistdet As New ArrayList
    Public acclistdetpost As New ArrayList
    Public srevs As New ArrayList
    Public exps As New ArrayList
    Public tcasts As New ArrayList
    Public tncasts As New ArrayList
    Public tclbs As New ArrayList
    Public tnclbs As New ArrayList
    Public oc As New ArrayList
    Public od As New ArrayList
    Public cogs As New ArrayList
    Public ctr As New ArrayList
    Public tcnca As New ArrayList
    Public tcca As New ArrayList
    Public tcncl As New ArrayList
    Public tccl As New ArrayList
    Public addonlist As New ArrayList
    Public reval As New ArrayList
    Public closer As New ArrayList
    Public macro As New ArrayList
    Public neti As Decimal
    Public jcount As Integer = 0
    Public savetext As String
    Public filepath As String
    Public ostart As String
    Public bname As String
    Public dtype As Integer = 0
    Public edate As Date
    Public cudate As Date
    Public etext As String
    Public log As String = "IVACT LOG " & Date.Today
    Sub listening()
        Listener.Start()
    End Sub
    Sub resetarrays()
        srevs.Clear()
        exps.Clear()
        tcasts.Clear()
        tncasts.Clear()
        tclbs.Clear()
        tnclbs.Clear()
        oc.Clear()
        od.Clear()
        ctr.Clear()
        cogs.Clear()
        tcnca.Clear()
        tcca.Clear()
        tcncl.Clear()
        tccl.Clear()
        closer.Clear()
        reval.Clear()
    End Sub
    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles tsae.Click
        AddJ.Show()
    End Sub
    Sub wl(ByVal az As String)
        log = log & vbNewLine & az
        consolas.log.Clear()
        consolas.log.AppendText(log)
    End Sub
    Sub eexcel(ByVal z As Integer)
        If z = 4 Then
            Dim dlg As New SaveFileDialog
            dlg.Filter = "IVACT (*.ivact)|*.ivact"
            dlg.Title = "Save Balance Sheet Output"
            If dlg.ShowDialog = DialogResult.OK Then
                Dim bze As String = "[BALANCESHEET]"
                bze = bze & vbNewLine & "<H1>" & bs_date.Text
                bze = bze & vbNewLine & "<H2>" & bs_name.Text
                For i = 0 To dg.Rows.Count - 1
                    bze = bze & vbNewLine & "<LI>" & dg.Rows(i).Cells(0).Value & "/" & dg.Rows(i).Cells(1).Value & "/" & dg.Rows(i).Cells(2).Value & "/" & dg.Rows(i).Cells(3).Value
                Next
                File.WriteAllText(dlg.FileName, bze)
                wl("Export to Excel: Balance Sheet")
            End If
        ElseIf z = 3 Then
            Dim dlg As New SaveFileDialog
            dlg.Filter = "IVACT (*.ivact)|*.ivact"
            dlg.Title = "Save Income Statement Output"
            If dlg.ShowDialog = DialogResult.OK Then
                Dim icse As String = "[INCOMESTATEMENT]"
                icse = icse & vbNewLine & "<H1>" & is_date.Text
                icse = icse & vbNewLine & "<H2>" & is_name.Text
                For i = 0 To is_dg.Rows.Count - 1
                    icse = icse & vbNewLine & "<LI>" & is_dg.Rows(i).Cells(0).Value & "/" & is_dg.Rows(i).Cells(1).Value & "/" & is_dg.Rows(i).Cells(2).Value & "/" & is_dg.Rows(i).Cells(3).Value
                Next
                File.WriteAllText(dlg.FileName, icse)
                wl("Export to Excel: Income Statement")
            End If
        ElseIf z = 2 Then
            Dim dlg As New SaveFileDialog
            dlg.Filter = "IVACT (*.ivact)|*.ivact"
            dlg.Title = "Save Trial Balance Output"
            If dlg.ShowDialog = DialogResult.OK Then
                Dim tbale As String = "[TRIALBALANCE]"
                tbale = tbale & vbNewLine & "<H1>" & tb_date.Text
                tbale = tbale & vbNewLine & "<H2>" & tb_name.Text
                For i = 0 To tbal_dg.Rows.Count - 1
                    tbale = tbale & vbNewLine & "<LI>" & tbal_dg.Rows(i).Cells(0).Value & "/" & tbal_dg.Rows(i).Cells(1).Value & "/" & tbal_dg.Rows(i).Cells(2).Value
                Next
                File.WriteAllText(dlg.FileName, tbale)
                wl("Export to Excel: Trial Balance")
            End If
        ElseIf z = 1 Then
            Dim dlg As New SaveFileDialog
            dlg.Filter = "IVACT (*.ivact)|*.ivact"
            dlg.Title = "Save Ledger Output"
            If dlg.ShowDialog = DialogResult.OK Then
                Dim lee As String = "[LEDGER]"
                For Each myuc As tricolledger In l_field.Controls
                    lee = lee & vbNewLine & myuc.dumpcode()
                Next
                File.WriteAllText(dlg.FileName, lee)
                wl("Export to Excel: Ledger")
            End If
        ElseIf z = 0 Then
            MsgBox("Please use standard save function found under File menu", MsgBoxStyle.OkCancel, "Errr...")
        End If
    End Sub
#Region "Export"
    Sub exportledger2(ByVal ty As Integer)
        l_field.Controls.Clear()
        If ty = 1 Then
            For Each item As String In acclist.ToArray
                If Not dsub(item) = 17 Then
                    Dim myuc As New tricolledger
                    myuc.Dock = DockStyle.Top
                    myuc.LName.Text = item
                    myuc.Name = item
                    If returnopenbal(item) < 0 Then
                        If rtype(item) = 1 Then
                            myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item) * -1, ""})
                        Else
                            myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item) * -1})
                        End If
                    ElseIf returnopenbal(item) > 0 Then
                        If rtype(item) = 1 Then
                            myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item)})
                        Else
                            myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item), ""})
                        End If
                    End If
                    For Each sn As journals In field.Controls.OfType(Of journals)
                        For i As Integer = 0 To sn.dg.RowCount - 1
                            If sn.dg.Rows(i).Cells(1).Value = item Then
                                Dim z As String = sn.dg.Rows(sn.dg.Rows.Count - 1).Cells(1).Value.ToString.Replace("(", "")
                                Dim ze As String = z.Replace(")", "")
                                If Not sn.dg.Rows(i).Cells(2).Value = "" Then
                                    myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, sn.dg.Rows(i).Cells(2).Value, ""})
                                Else
                                    myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, "", sn.dg.Rows(i).Cells(3).Value, sn.dg.Rows(i).Cells(3).Value * -1})
                                End If
                            End If
                        Next
                    Next
                    myuc.selfbal()
                    l_field.Controls.Add(myuc)
                    myuc.dg.ClearSelection()
                    wl("Compute ledger: " & myuc.Name)
                End If
            Next
        End If
    End Sub
    Sub exporttbal2()
        tbal_dg.Rows.Clear()
        For Each item As String In acclist.ToArray
            If Not dsub(item) = 17 Then
                Dim myuc As New tricolledger
                myuc.Dock = DockStyle.Top
                myuc.LName.Text = item
                myuc.Name = item
                If returnopenbal(item) < 0 Then
                    If rtype(item) = 1 Then
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item) * -1, ""})
                    Else
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item) * -1})
                    End If
                Else
                    If rtype(item) = 1 Then
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item)})
                    Else
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item), ""})
                    End If
                End If
                For Each sn As journals In field.Controls.OfType(Of journals)
                    For i As Integer = 0 To sn.dg.RowCount - 1
                        If sn.dg.Rows(i).Cells(1).Value = item Then
                            Dim z As String = sn.dg.Rows(sn.dg.Rows.Count - 1).Cells(1).Value.ToString.Replace("(", "")
                            Dim ze As String = z.Replace(")", "")
                            If Not sn.dg.Rows(i).Cells(2).Value = "" Then
                                myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, sn.dg.Rows(i).Cells(2).Value, ""})
                            Else
                                myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, "", sn.dg.Rows(i).Cells(3).Value, sn.dg.Rows(i).Cells(3).Value * -1})
                            End If
                        End If
                    Next
                Next
                myuc.selfbal()
                Dim lz As Integer = myuc.dg.Rows.Count - 1
                Dim lastbal As Decimal = myuc.dg.Rows(lz).Cells(5).Value
                If rtype(item) = 1 Then
                    If lastbal < 0 Then
                        tbal_dg.Rows.Add(New String() {item, Decimal.Parse(lastbal) * -1, ""})
                    Else
                        tbal_dg.Rows.Add(New String() {item, "", Decimal.Parse(lastbal)})
                    End If
                Else
                    If lastbal < 0 Then
                        tbal_dg.Rows.Add(New String() {item, "", Decimal.Parse(lastbal) * -1})
                    Else
                        tbal_dg.Rows.Add(New String() {item, Decimal.Parse(lastbal), ""})
                    End If
                End If
            End If
        Next
        Dim rd As Decimal
        Dim rc As Decimal
        For i As Integer = 0 To tbal_dg.RowCount - 1
            On Error Resume Next
            rd += tbal_dg.Rows(i).Cells(1).Value
            rc += tbal_dg.Rows(i).Cells(2).Value
        Next
        tbal_dg.Rows.Add(New String() {"Total", rd, rc})
        tbal_dg.ClearSelection()
        tbal_dg.Rows(tbal_dg.Rows.Count - 1).Selected = True
    End Sub
    Function returncbal(ByVal ss As String)
        For Each item As String In acclist.ToArray
            If item = ss Then
                If Not dsub(item) = 17 Then
                    Dim myuc As New tricolledger
                    myuc.Dock = DockStyle.Top
                    myuc.LName.Text = item
                    myuc.Name = item
                    If returnopenbal(item) < 0 Then
                        If rtype(item) = 1 Then
                            myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item) * -1, ""})
                        Else
                            myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item) * -1})
                        End If
                    Else
                        If rtype(item) = 1 Then
                            myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item)})
                        Else
                            myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item), ""})
                        End If
                    End If
                    For Each sn As journals In field.Controls.OfType(Of journals)
                        For i As Integer = 0 To sn.dg.RowCount - 1
                            If sn.dg.Rows(i).Cells(1).Value = item Then
                                Dim z As String = sn.dg.Rows(sn.dg.Rows.Count - 1).Cells(1).Value.ToString.Replace("(", "")
                                Dim ze As String = z.Replace(")", "")
                                If Not sn.dg.Rows(i).Cells(2).Value = "" Then
                                    myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, sn.dg.Rows(i).Cells(2).Value, ""})
                                Else
                                    myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, "", sn.dg.Rows(i).Cells(3).Value, sn.dg.Rows(i).Cells(3).Value * -1})
                                End If
                            End If
                        Next
                    Next
                    myuc.selfbal()
                    Dim lz As Integer = myuc.dg.Rows.Count - 1
                    Dim lastbal As Decimal = myuc.dg.Rows(lz).Cells(5).Value
                    If rtype(item) = 1 Then
                        If lastbal < 0 Then
                            Return (Decimal.Parse(lastbal) * -1)
                        Else
                            Return Decimal.Parse(lastbal)
                        End If
                    Else
                        If lastbal < 0 Then
                            Return (Decimal.Parse(lastbal) * -1)
                        Else
                            Return Decimal.Parse(lastbal)
                        End If
                    End If
                End If
            End If
        Next
    End Function
    Function rtype(ByVal item As String) As Integer
        If (dsub(item) = 1 Or dsub(item) = 2 Or dsub(item) = 6 Or dsub(item) = 7 Or dsub(item) = 8 Or dsub(item) = 10 Or dsub(item) = 12 Or dsub(item) = 15 Or dsub(item) = 16) Or dsub(item) = 18 Then
            Return 1
        Else
            Return 2
        End If
    End Function
    Sub exportis2()
        resetarrays()
        is_dg.Rows.Clear()
        For Each item As String In acclist.ToArray
            If Not dsub(item) = 17 Then
                Dim myuc As New tricolledger
                myuc.Dock = DockStyle.Top
                myuc.LName.Text = item
                myuc.Name = item
                If returnopenbal(item) < 0 Then
                    If rtype(item) = 1 Then
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item) * -1, ""})
                    Else
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item) * -1})
                    End If
                Else
                    If rtype(item) = 1 Then
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item)})
                    Else
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item), ""})
                    End If
                End If
                For Each sn As journals In field.Controls.OfType(Of journals)
                    For i As Integer = 0 To sn.dg.RowCount - 1
                        If sn.dg.Rows(i).Cells(1).Value = item Then
                            Dim z As String = sn.dg.Rows(sn.dg.Rows.Count - 1).Cells(1).Value.ToString.Replace("(", "")
                            Dim ze As String = z.Replace(")", "")
                            If Not sn.dg.Rows(i).Cells(2).Value = "" Then
                                myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, sn.dg.Rows(i).Cells(2).Value, ""})
                            Else
                                myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, "", sn.dg.Rows(i).Cells(3).Value, sn.dg.Rows(i).Cells(3).Value * -1})
                            End If
                        End If
                    Next
                Next
                myuc.selfbal()
                Dim lz As Integer = myuc.dg.Rows.Count - 1
                Dim lastbal As Decimal = myuc.dg.Rows(lz).Cells(5).Value
                If dsub(item) = 1 Then
                    is_dg.Rows.Add(New String() {item, "", "", lastbal})
                End If
                If dsub(item) = 14 Then
                    Dim z(2) As String
                    z(0) = item
                    z(1) = lastbal
                    ctr.Add(z)
                End If
                If dsub(item) = 13 Then
                    Dim z(2) As String
                    z(0) = item
                    z(1) = lastbal
                    cogs.Add(z)
                End If
                If dsub(item) = 3 Then
                    Dim z(4) As String
                    z(0) = item
                    z(3) = lastbal
                    exps.Add(z)
                End If
                If dsub(item) = 2 Then
                    Dim z(4) As String
                    z(0) = item
                    z(3) = lastbal
                    srevs.Add(z)
                End If
            End If
        Next
        Dim pcbal As Decimal
        Dim tctr As Decimal
        For Each zyyz As String() In ctr
            is_dg.Rows.Add(New String() {zyyz(0), "", "", zyyz(1)})
            tctr += Decimal.Parse(zyyz(1))
        Next
        Try
            pcbal = is_dg.Rows(0).Cells(3).Value + (tctr * -1)
        Catch ex As Exception
            MsgBox("Main Revenue account type not found.", MsgBoxStyle.OkOnly, "Errr...")
            wl("Export Income Statement Error: Main Revenue account type not found.")
            Exit Sub
        End Try
        is_dg.Rows.Add(New String() {"Nett Sales", "", "", pcbal})
        Dim tcogs As Decimal
        For Each ty As String() In cogs
            is_dg.Rows.Add(New String() {ty(0), "", "", ty(1)})
            tcogs += Decimal.Parse(ty(1))
        Next
        pcbal += (tcogs * -1)
        is_dg.Rows.Add(New String() {"Gross Profit", "", "", pcbal})
        is_dg.Rows.Add("")
        Dim texps As Decimal
        For Each yt As String() In exps
            is_dg.Rows.Add(New String() {yt(0), "", (yt(3)), ""})
            texps += Decimal.Parse(yt(3))
        Next
        is_dg.Rows.Add(New String() {"Total Expenses", "", "", texps})
        is_dg.Rows.Add(New String() {"", "", "", pcbal - texps})
        is_dg.Rows.Add("")
        Dim tsrevs As Decimal
        For Each tyt As String() In srevs
            is_dg.Rows.Add(New String() {tyt(0), "", (tyt(3)), ""})
            tsrevs += Decimal.Parse(tyt(3))
        Next
        is_dg.Rows.Add(New String() {"Total Side Income", "", "", tsrevs})
        is_dg.Rows.Add("")
        is_dg.Rows.Add(New String() {"Nett Profit", "", "", pcbal - texps + tsrevs})
        is_dg.ClearSelection()
        is_dg.Rows(is_dg.Rows.Count - 1).Selected = True
        neti = pcbal - texps + tsrevs
    End Sub
    Sub closerr()
        resetarrays()
        For Each item As String In acclist.ToArray
            If Not dsub(item) = 17 Then
                Dim myuc As New tricolledger
                myuc.Dock = DockStyle.Top
                myuc.LName.Text = item
                myuc.Name = item
                If returnopenbal(item) < 0 Then
                    If rtype(item) = 1 Then
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item) * -1, ""})
                    Else
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item) * -1})
                    End If
                Else
                    If rtype(item) = 1 Then
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item)})
                    Else
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item), ""})
                    End If
                End If
                For Each sn As journals In field.Controls.OfType(Of journals)
                    For i As Integer = 0 To sn.dg.RowCount - 1
                        If sn.dg.Rows(i).Cells(1).Value = item Then
                            Dim z As String = sn.dg.Rows(sn.dg.Rows.Count - 1).Cells(1).Value.ToString.Replace("(", "")
                            Dim ze As String = z.Replace(")", "")
                            If Not sn.dg.Rows(i).Cells(2).Value = "" Then
                                myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, sn.dg.Rows(i).Cells(2).Value, ""})
                            Else
                                myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, "", sn.dg.Rows(i).Cells(3).Value, sn.dg.Rows(i).Cells(3).Value * -1})
                            End If
                        End If
                    Next
                Next
                myuc.selfbal()
                Dim lz As Integer = myuc.dg.Rows.Count - 1
                Dim lastbal As Decimal = myuc.dg.Rows(lz).Cells(5).Value
                If dsub(item) = 1 Then
                    is_dg.Rows.Add(New String() {item, "", "", lastbal})
                End If
                If dsub(item) = 14 Then
                    Dim z(2) As String
                    z(0) = item
                    z(1) = lastbal
                    ctr.Add(z)
                End If
                If dsub(item) = 13 Then
                    Dim z(2) As String
                    z(0) = item
                    z(1) = lastbal
                    cogs.Add(z)
                End If
                If dsub(item) = 3 Then
                    Dim z(4) As String
                    z(0) = item
                    z(3) = lastbal
                    exps.Add(z)
                End If
                If dsub(item) = 2 Then
                    Dim z(4) As String
                    z(0) = item
                    z(3) = lastbal
                    srevs.Add(z)
                End If
            End If
        Next
        Dim pcbal As Decimal
        Dim tctr As Decimal
        For Each zyyz As String() In ctr
            is_dg.Rows.Add(New String() {zyyz(0), "", "", zyyz(1)})
            tctr += Decimal.Parse(zyyz(1))
        Next
        Try
            pcbal = is_dg.Rows(0).Cells(3).Value + (tctr * -1)
        Catch ex As Exception
            MsgBox("Main Revenue account type not found.", MsgBoxStyle.OkOnly, "Errr...")
            wl("Closing Error: Main Revenue account type not found.")
            Exit Sub
        End Try
        is_dg.Rows.Add(New String() {"Nett Sales", "", "", pcbal})
        Dim tcogs As Decimal
        For Each ty As String() In cogs
            is_dg.Rows.Add(New String() {ty(0), "", "", ty(1)})
            tcogs += Decimal.Parse(ty(1))
        Next
        pcbal += (tcogs * -1)
        is_dg.Rows.Add(New String() {"Gross Profit", "", "", pcbal})
        dg.Rows.Add("")
        Dim texps As Decimal
        For Each yt As String() In exps
            is_dg.Rows.Add(New String() {yt(0), "", (yt(3)), ""})
            texps += Decimal.Parse(yt(3))
        Next
        is_dg.Rows.Add(New String() {"Total Expenses", "", "", texps})
        is_dg.Rows.Add(New String() {"", "", "", pcbal - texps})
        dg.Rows.Add("")
        Dim tsrevs As Decimal
        For Each tyt As String() In srevs
            is_dg.Rows.Add(New String() {tyt(0), "", (tyt(3)), ""})
            tsrevs += Decimal.Parse(tyt(3))
        Next
        is_dg.Rows.Add(New String() {"Total Side Income", "", "", tsrevs})
        dg.Rows.Add("")
        is_dg.Rows.Add(New String() {"Nett Profit", "", "", pcbal - texps + tsrevs})
        is_dg.ClearSelection()
        is_dg.Rows(is_dg.Rows.Count - 1).Selected = True
        neti = pcbal - texps + tsrevs
        Dim zaa2(2) As String
        zaa2(0) = is_dg.Rows(0).Cells(0).Value
        zaa2(1) = is_dg.Rows(0).Cells(3).Value
        Dim bz1 As New ArrayList
        bz1.Add(zaa2)
        preparere(0, "rev", bz1)
        preparere(0, "srev", srevs)
        preparere(0, "crev", ctr)
        preparere(0, "exp", exps)
        preparere(0, "cogs", cogs)
        preparere(0, "neti", Nothing)
        preparere(1, "", Nothing)
        updateallbalance()
        Dim dlg As New SaveFileDialog
        dlg.Title = "Close project as..."
        dlg.Filter = "IVACT (*.ivact)|*.ivact"
        If dlg.ShowDialog = DialogResult.OK Then
            savetext = ""
            For Each azz As String In acclistdetpost.Cast(Of String)()
                wst("<AC>" & azz)
            Next
            wst("<BN>" & bname)
            wst("<DT>" & dtype)
            wst("<CD>" & cudate)
            wst("<ED>" & edate)
            For Each zee As String In macro
                wst(zee)
            Next
            File.WriteAllText(dlg.FileName, savetext)
        End If
    End Sub
    Sub exportbalsheet2(ByVal asses As Boolean, ByVal liab As Boolean, ByVal oeq As Boolean)
        resetarrays()
        dg.Rows.Clear()
        For Each item As String In acclist.ToArray
            If Not dsub(item) = 17 Then
                Dim myuc As New tricolledger
                myuc.Dock = DockStyle.Top
                myuc.LName.Text = item
                myuc.Name = item
                If returnopenbal(item) < 0 Then
                    If rtype(item) = 1 Then
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item) * -1, ""})
                    Else
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item) * -1})
                    End If
                Else
                    If rtype(item) = 1 Then
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item)})
                    Else
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item), ""})
                    End If
                End If
                For Each sn As journals In field.Controls.OfType(Of journals)
                    For i As Integer = 0 To sn.dg.RowCount - 1
                        If sn.dg.Rows(i).Cells(1).Value = item Then
                            Dim z As String = sn.dg.Rows(sn.dg.Rows.Count - 1).Cells(1).Value.ToString.Replace("(", "")
                            Dim ze As String = z.Replace(")", "")
                            If Not sn.dg.Rows(i).Cells(2).Value = "" Then
                                myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, sn.dg.Rows(i).Cells(2).Value, ""})
                            Else
                                myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, "", sn.dg.Rows(i).Cells(3).Value, sn.dg.Rows(i).Cells(3).Value * -1})
                            End If
                        End If
                    Next
                Next
                myuc.selfbal()
                Dim lz As Integer = myuc.dg.Rows.Count - 1
                Dim lastbal As Decimal = myuc.dg.Rows(lz).Cells(5).Value
                If dsub(item) = 4 Then
                    Dim z(4) As String
                    z(0) = item
                    z(3) = lastbal
                    tncasts.Add(z)
                End If
                If (dsub(item) = 5 Or dsub(item) = 11) Then
                    Dim z(4) As String
                    z(0) = item
                    z(3) = lastbal
                    tcasts.Add(z)
                End If
                If dsub(item) = 6 Then
                    Dim z(4) As String
                    z(0) = item
                    z(3) = lastbal
                    tnclbs.Add(z)
                End If
                If (dsub(item) = 7 Or dsub(item) = 12 Or dsub(item) = 10) Then
                    Dim z(4) As String
                    z(0) = item
                    z(3) = lastbal
                    tclbs.Add(z)
                End If
                If dsub(item) = 8 Then
                    Dim z(2) As String
                    z(0) = item
                    z(1) = lastbal
                    oc.Add(z)
                End If
                If dsub(item) = 9 Then
                    Dim z(2) As String
                    z(0) = item
                    z(1) = lastbal
                    od.Add(z)
                End If
                If dsub(item) = 15 Then
                    Dim z(2) As String
                    z(0) = item
                    z(1) = lastbal
                    tcca.Add(z)
                End If
                If dsub(item) = 16 Then
                    Dim z(2) As String
                    z(0) = item
                    z(1) = lastbal
                    tcnca.Add(z)
                End If
                If dsub(item) = 18 Then
                    Dim z(2) As String
                    z(0) = item
                    z(1) = lastbal
                    reval.Add(z)
                End If
                If dsub(item) = 19 Then
                    Dim z(2) As String
                    z(0) = item
                    z(1) = lastbal
                    tccl.Add(z)
                End If
                If dsub(item) = 20 Then
                    Dim z(2) As String
                    z(0) = item
                    z(1) = lastbal
                    tcncl.Add(z)
                End If
            End If
        Next
        Dim pbcal As Decimal
        Dim pbcal2 As Decimal
        If asses = True Then
            dg.Rows.Add("Non-current Assets")
            Dim tncasets As Decimal
            For Each yt As String() In tncasts
                dg.Rows.Add(New String() {yt(0), (yt(3)), "", ""})
                tncasets += Decimal.Parse(yt(3))
            Next
            Dim tcncasets As Decimal
            For Each yt As String() In tcnca
                dg.Rows.Add(New String() {yt(0), (yt(1)), "", ""})
                tcncasets += Decimal.Parse(yt(1))
            Next
            pbcal = tncasets - tcncasets
            dg.Rows.Add(New String() {"Total Non-current Assets", "", pbcal, ""})
            dg.Rows.Add("")
            dg.Rows.Add("Current Assets")
            Dim tcasets As Decimal
            For Each yt As String() In tcasts
                dg.Rows.Add(New String() {yt(0), (yt(3)), "", ""})
                tcasets += Decimal.Parse(yt(3))
            Next
            Dim tccasets As Decimal
            For Each yt As String() In tcca
                dg.Rows.Add(New String() {yt(0), (yt(1)), "", ""})
                tccasets += Decimal.Parse(yt(1))
            Next
            dg.Rows.Add(New String() {"Total Current Assets", "", tcasets - tccasets, ""})
            dg.Rows.Add("")
            pbcal += tcasets - tccasets
            dg.Rows.Add(New String() {"Total Assets", "", "", pbcal})
            dg.Rows.Add("")
        End If
        If liab = True Then
            dg.Rows.Add("Non-current Liabilities")
            Dim tnclubs As Decimal
            For Each yt As String() In tnclbs
                dg.Rows.Add(New String() {yt(0), (yt(3)), "", ""})
                tnclubs += Decimal.Parse(yt(3))
            Next
            Dim tcnclubs As Decimal
            For Each yt As String() In tcncl
                dg.Rows.Add(New String() {yt(0), (yt(1)), "", ""})
                tcnclubs += Decimal.Parse(yt(1))
            Next
            pbcal2 = tnclubs - tcnclubs
            dg.Rows.Add(New String() {"Total Non-current Liabilities", "", pbcal2, ""})
            dg.Rows.Add("")
            dg.Rows.Add("Current Liabilities")
            Dim tclubs As Decimal
            For Each yt As String() In tclbs
                dg.Rows.Add(New String() {yt(0), (yt(3)), "", ""})
                tclubs += Decimal.Parse(yt(3))
            Next
            Dim tcclubs As Decimal
            For Each yt As String() In tccl
                dg.Rows.Add(New String() {yt(0), (yt(1)), "", ""})
                tcclubs += Decimal.Parse(yt(1))
            Next
            pbcal2 += tclubs - tcclubs
            dg.Rows.Add(New String() {"Total Current Liabilities", "", tclubs - tcclubs, ""})
            dg.Rows.Add("")
            dg.Rows.Add(New String() {"Total Liabilities", "", "", pbcal2})
            dg.Rows.Add("")
            pbcal -= pbcal2
            dg.Rows.Add(New String() {"Total Assets - Total Liabilities", "", "", pbcal})
            dg.ClearSelection()
            dg.Rows(dg.Rows.Count - 1).Selected = True
            dg.Rows.Add("")
        End If
        If oeq = True Then
            dg.Rows.Add("Owner's Equity")
            Dim toc As Decimal
            Dim toc1 As Decimal
            For Each yt As String() In oc
                dg.Rows.Add(New String() {yt(0), "", "", (yt(1))})
                toc += Decimal.Parse(yt(1))
            Next
            toc1 = toc
            For Each zeea As String() In reval
                dg.Rows.Add(New String() {zeea(0), "", "", zeea(1)})
                toc += Decimal.Parse(zeea(1))
            Next
            If Not neti = 0 Then
                dg.Rows.Add(New String() {"Income(Loss)", "", "", neti})
                toc += neti
            End If
            dg.Rows.Add(New String() {"", "", "", toc})
            Dim tod As Decimal
            For Each yt As String() In od
                dg.Rows.Add(New String() {yt(0), "", "", (yt(1))})
                tod += Decimal.Parse(yt(1))
            Next
            dg.Rows.Add(New String() {"", "", "", toc - tod})
            dg.Rows(dg.Rows.Count - 1).Selected = True
            If Not toc - tod = pbcal Then
                If asses = True And liab = True Then
                    MsgBox("Unable to balance, try exporting to Income Statement first.", MsgBoxStyle.OkOnly, "Errr...")
                    wl("Balance Sheet Error: Unable to balance.")
                    Exit Sub
                End If
            End If
        End If
    End Sub
#End Region
    Public Function dsub(ByVal acname As String)
        For Each item As String In acclistdet.ToArray
            Dim azz As String() = item.Split("/")
            If azz(0) = acname Then
                If azz(1) = "Revenue (Main)" Then Return 1
                If azz(1) = "Revenue (Side)" Then Return 2
                If azz(1) = "Expenses" Then Return 3
                If azz(1) = "Assets (Non-current)" Then Return 4
                If azz(1) = "Assets (Current)" Then Return 5
                If azz(1) = "Liabilities (Non-current)" Then Return 6
                If azz(1) = "Liabilities (Current)" Then Return 7
                If azz(1) = "Owner's Equity (Capital)" Then Return 8
                If azz(1) = "Owner's Equity (Drawing)" Then Return 9
                If azz(1) = "Unearned Revenue" Then Return 10
                If azz(1) = "Prepaid Expenses" Then Return 11
                If azz(1) = "Accrued Expenses" Then Return 12
                If azz(1) = "Cost of Goods Sold" Then Return 13
                If azz(1) = "Contra (Revenue)" Then Return 14
                If azz(1) = "Contra (Current Assets)" Then Return 15
                If azz(1) = "Contra (Non-current Assets)" Then Return 16
                If azz(1) = "Income Summary" Then Return 17
                If azz(1) = "Retained Earnings" Then Return 18
                If azz(1) = "Contra (Current Liabilities)" Then Return 19
                If azz(1) = "Contra (Non-current Liabilities)" Then Return 20
            End If
        Next
    End Function

    Private Sub TrialBalanceToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TrialBalanceToolStripMenuItem.Click
        If Not mtb.SelectedIndex = 2 Then
            mtb.SelectedIndex = 2
        Else
            exporttbal2()
            updata()
        End If
    End Sub

    Private Sub IncomeStatementToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles IncomeStatementToolStripMenuItem.Click
        If Not mtb.SelectedIndex = 3 Then
            mtb.SelectedIndex = 3
        Else
            exportis2()
            updata()
        End If
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        If checkchanges() = False Then
            End
        Else
            Dim dlg As New changedetect
            If dlg.ShowDialog = MsgBoxResult.Ok Then
                End
            ElseIf MsgBoxResult.Yes Then
                ToolStripMenuItem6.PerformClick()
                End
            ElseIf MsgBoxResult.Cancel Then
                Exit Sub
            End If
        End If
    End Sub

    Private Sub SaveToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles stsm.Click
        Dim dlg As New SaveFileDialog
        dlg.Title = "Save project as..."
        dlg.Filter = "IVACT (*.ivact)|*.ivact"
        If dlg.ShowDialog = DialogResult.OK Then
            savetext = ""
            For Each azz As String In acclistdet.Cast(Of String)()
                wst("<AC>" & azz)
            Next
            For i As Integer = 0 To jcount
                For Each sn As journals In field.Controls.OfType(Of journals)
                    If sn.Name = i.ToString("D4") Then savetext = savetext & vbNewLine & sn.dumpcode()
                Next
            Next
            wst("<BN>" & bname)
            wst("<DT>" & dtype)
            wst("<CD>" & cudate)
            wst("<ED>" & edate)
            File.WriteAllText(dlg.FileName, savetext)
            filepath = Path.GetFileNameWithoutExtension(dlg.FileName)
            l_pro.Text = filepath
            filep = dlg.FileName
        End If
    End Sub
    Sub wst(ByVal s As String)
        savetext = savetext & vbNewLine & s
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        about.Show()
    End Sub

    Private Sub OpenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenToolStripMenuItem.Click
        Dim dlg As New OpenFileDialog
        dlg.Title = "Open saved projects"
        dlg.Filter = "IVACT (*.ivact)|*.ivact"
        If dlg.ShowDialog = DialogResult.OK Then
            mtb.SelectedIndex = 0
            openfile(dlg.FileName)
        End If
    End Sub
#Region "Load file"
    Sub openfile(ByVal str As String)
        jcount = 0
        acclist.Clear()
        acclistdet.Clear()
        field.Controls.Clear()
        macro.Clear()
        refreshmacros()
        bname = ""
        dtype = 0
        edate = Nothing
        cudate = Nothing
        neti = 0
        Dim FileName3 As String = str
        parsefile(File.ReadAllLines(FileName3).ToArray)
        filepath = Path.GetFileNameWithoutExtension(str)
        l_pro.Text = filepath
        filep = str
        wl("Open file: " & filepath)
    End Sub
    Sub parsefile(ByVal az As Array)
        Dim cname As String
        For Each line As String In az
            If line.Contains("<M>") Then
                ts_mac.DropDownItems.Add(line.Replace("<M>", ""))
                macro.Add(line)
                If ts_sm.Visible = False Then ts_sm.Visible = True
            End If
            If line.Contains("<MD>") Or line.Contains("<MC>") Or line.Contains("<MDE>") Or line.Contains("</M>") Then
                macro.Add(line)
            End If
            If line.Contains("<J>") Then
                Dim myuc As New journals
                myuc.Dock = DockStyle.Top
                myuc.JNo.Text = "Journal No: " & line.Replace("<J>", "")
                cname = line.Replace("<J>", "")
                myuc.Name = cname
                field.Controls.Add(myuc)
                field.Controls.SetChildIndex(myuc, 0)
                Dim z As Integer = CInt(cname)
                jcount = CInt(z.ToString("D1")) + 1
            End If
            If line.Contains("<D>") Then
                Dim zzz As String = line.Replace("<D>", "")
                Dim azzz As String() = zzz.Split("/")
                For Each zz As journals In field.Controls.OfType(Of journals)
                    If zz.Name = cname Then
                        zz.dg.Rows.Add(New String() {azzz(0), azzz(1), azzz(2), ""})
                    End If
                Next
            End If
            If line.Contains("<C>") Then
                Dim zzz As String = line.Replace("<C>", "")
                Dim azzz As String() = zzz.Split("/")
                For Each zz As journals In field.Controls.OfType(Of journals)
                    If zz.Name = cname Then
                        zz.dg.Rows.Add(New String() {azzz(0), azzz(1), "", azzz(2)})
                    End If
                Next
            End If
            If line.Contains("<DE>") Then
                Dim zzz As String = line.Replace("<DE>", "")
                For Each zz As journals In field.Controls.OfType(Of journals)
                    If zz.Name = cname Then
                        zz.dg.Rows.Add(New String() {"", "(" & zzz & ")"})
                    End If
                Next
            End If
            If line.Contains("</J>") Then
                For Each zz As journals In field.Controls.OfType(Of journals)
                    If zz.Name = cname Then
                        zz.dg.ClearSelection()
                    End If
                Next
                cname = ""
            End If
            If line.Contains("<AC>") Then
                Dim azzzz As String = line.Replace("<AC>", "")
                Dim aaz As String() = azzzz.Split("/")
                If Not acclist.Contains(aaz(0)) Then
                    acclist.Add(aaz(0))
                    acclistdet.Add(azzzz)
                End If
            End If
            If line.Contains("<BN>") Then
                Dim bzzz As String = line.Replace("<BN>", "")
                bname = bzzz
            End If
            If line.Contains("<DT>") Then
                Dim azz As String = line.Replace("<DT>", "")
                dtype = CInt(azz)
            End If
            If line.Contains("<CD>") Then
                Dim azz As String = line.Replace("<CD>", "")
                cudate = Date.Parse(azz)
            End If
            If line.Contains("<ED>") Then
                Dim azz As String = line.Replace("<ED>", "")
                edate = Date.Parse(azz)
            End If
        Next
        If macro.Count = 0 Then ts_sm.Visible = False
    End Sub
#End Region
    Function returnopenbal(ByVal z As String)
        For Each item As String In acclistdet.ToArray
            Dim azz As String() = item.Split("/")
            If azz(0) = z Then
                Dim az As String() = item.Split("/")
                If az.Count = 2 Then
                    Return 0
                Else
                    Return az(2)
                End If
            End If
        Next
    End Function

    Private Sub ManageToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ManageToolStripMenuItem.Click
        manAcc.Show()
    End Sub
    Private Sub FullToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FullToolStripMenuItem.Click
        If Not mtb.SelectedIndex = 4 Then
            mtb.SelectedIndex = 4
        Else
            exportbalsheet2(True, True, True)
            updata()
        End If
    End Sub

    Private Sub PartialToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PartialToolStripMenuItem.Click
        pbs.Show()
    End Sub

    Private Sub CalculatorsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CalculatorsToolStripMenuItem.Click
        MultiCalc.Show()
    End Sub

    Private Sub LedgerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LedgerToolStripMenuItem.Click
        If Not mtb.SelectedIndex = 1 Then
            mtb.SelectedIndex = 1
        Else
            exportledger2(1)
            t_q.Clear()
        End If
    End Sub

    Private Sub DocumentationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DocumentationToolStripMenuItem.Click
        Process.Start("http://ivansaccountingtools.sourceforge.net/docs/")
    End Sub
    Sub preparere(ByVal cmd As String, ByVal typae As String, ByVal zdat As ArrayList)
        If cmd = 0 Then
            Dim zdate As Date
            If dtype = 0 Then
                zdate = New Date(Date.Today.Year, 12, DateTime.DaysInMonth(Date.Today.Year, 12))
            Else
                zdate = edate
            End If
            Dim abz As String = zdate.ToLongDateString
            If closer.Count = 0 Then
                closer.Add("<J>" & jcount.ToString("D4"))
            End If
            If typae = "rev" Then
                For Each ze As String() In zdat
                    closer.Add("<D>" & abz & "/" & ze(0) & "/" & ze(1))
                Next
            ElseIf typae = "srev" Then
                For Each ze As String() In zdat
                    closer.Add("<D>" & abz & "/" & ze(0) & "/" & ze(3))
                Next
            ElseIf typae = "crev" Then
                For Each ze As String() In zdat
                    closer.Add("<C>" & abz & "/" & ze(0) & "/" & ze(1))
                Next
            ElseIf typae = "exp" Then
                For Each ze As String() In zdat
                    closer.Add("<C>" & abz & "/" & ze(0) & "/" & ze(3))
                Next
            ElseIf typae = "cogs" Then
                For Each ze As String() In zdat
                    closer.Add("<C>" & abz & "/" & ze(0) & "/" & ze(1))
                Next
            ElseIf typae = "neti" Then
                If neti > 0 Then
                    closer.Add("<C>" & abz & "/" & "Income Summary/" & neti)
                ElseIf neti < 0 Then
                    closer.Add("<D>" & abz & "/" & "Income Summary/" & (neti * -1))
                End If
            End If
        ElseIf cmd = 1 Then
            closer.Add("<DE>Closing all Revenues And Expenses to Income Summary.")
            closer.Add("</J>")
            parsefile(closer.ToArray)
            updatere()
        End If
    End Sub
    Sub updateallbalance()
        acclistdetpost.Clear()
        For Each item As String In acclist.ToArray
            If Not dsub(item) = 17 Then
                Dim myuc As New tricolledger
                myuc.Dock = DockStyle.Top
                myuc.LName.Text = item
                myuc.Name = item
                If returnopenbal(item) < 0 Then
                    If rtype(item) = 1 Then
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item) * -1, ""})
                    Else
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item) * -1})
                    End If
                Else
                    If rtype(item) = 1 Then
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", "", returnopenbal(item)})
                    Else
                        myuc.dg.Rows.Add(New String() {"", "Opening Balance", "", returnopenbal(item), ""})
                    End If
                End If
                For Each sn As journals In field.Controls.OfType(Of journals)
                    For i As Integer = 0 To sn.dg.RowCount - 1
                        If sn.dg.Rows(i).Cells(1).Value = item Then
                            Dim z As String = sn.dg.Rows(sn.dg.Rows.Count - 1).Cells(1).Value.ToString.Replace("(", "")
                            Dim ze As String = z.Replace(")", "")
                            If Not sn.dg.Rows(i).Cells(2).Value = "" Then
                                myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, sn.dg.Rows(i).Cells(2).Value, ""})
                            Else
                                myuc.dg.Rows.Add(New String() {sn.dg.Rows(i).Cells(0).Value, ze, sn.Name, "", sn.dg.Rows(i).Cells(3).Value, sn.dg.Rows(i).Cells(3).Value * -1})
                            End If
                        End If
                    Next
                Next
                myuc.selfbal()
                Dim lz As Integer = myuc.dg.Rows.Count - 1
                Dim lastbal As Decimal = myuc.dg.Rows(lz).Cells(5).Value
                updateobalance(myuc.Name, lastbal)
            End If
        Next
    End Sub
    Sub updateobalance(ByVal str As String, ByVal val As String)
        For Each itemz As String In acclistdet.ToArray
            If itemz.Contains(str) Then
                Dim z As String() = itemz.Split("/")
                acclistdetpost.Add(z(0) & "/" & z(1) & "/" & val)
            End If
        Next
    End Sub
    Sub updatere()
        If neti = 0 Then Exit Sub
        Dim zdate As Date
        If dtype = 0 Then
            zdate = New Date(Date.Today.Year, 12, DateTime.DaysInMonth(Date.Today.Year, 12))
        Else
            zdate = edate
        End If
        Dim abz As String = zdate.ToLongDateString
        Dim zz As New ArrayList
        Dim rename As String = "pholder"
        zz.Add("<J>" & jcount.ToString("D4"))
        If neti > 0 Then
            zz.Add("<D>" & abz & "/" & "Income Summary/" & neti)
        ElseIf neti < 0 Then
            zz.Add("<C>" & abz & "/" & "Income Summary/" & (neti * -1))
        End If
        For Each item As String In acclist.ToArray
            If dsub(item) = 18 Then
                rename = item
            End If
        Next
        If rename = "pholder" Then
            acclist.Add("Retained Earnings")
            acclistdet.Add("Retained Earnings/Retained Earnings/0")
            rename = "Retained Earnings"
        End If
        If neti > 0 Then
            zz.Add("<C>" & abz & "/" & rename & "/" & neti)
        ElseIf neti < 0 Then
            zz.Add("<D>" & abz & "/" & rename & "/" & (neti * -1))
        End If
        zz.Add("<DE>Update Retained Earnings balance.")
        zz.Add("</J>")
        parsefile(zz.ToArray)
    End Sub
#Region "Addons"
    Sub searchaddons()
        Dim strDLLs() As String
        If Directory.Exists(Application.StartupPath & "/Addons/") Then
            strDLLs = Directory.GetFileSystemEntries(Application.StartupPath & "/Addons/", "*.dll")
            If Not strDLLs.Length = 0 Then
                For Each mac As String In strDLLs
                    Try
                        genericparser(mac, 1)
                    Catch e As Exception
                        MsgBox(e.Message)
                    End Try
                Next
            End If
        End If
    End Sub
    Sub genericparser(ByVal alib As String, ByVal ins As Integer)
        Dim assembly__1 As Assembly = Assembly.LoadFrom(alib)
        Dim type As Type = assembly__1.GetType(Path.GetFileNameWithoutExtension(alib) & ".MainClass")
        Dim myObj As Object = Activator.CreateInstance(type)
        If ins = 1 Then
            Dim arguments As Object() = New Object() {1, 2}
            Dim result As Object
            result = type.InvokeMember("init", BindingFlags.[Default] Or BindingFlags.InvokeMethod, Nothing, myObj, arguments)
            prase(result, alib)
        ElseIf ins = 2 Then
            Dim arguments As Object() = New Object() {2, 3, jcount}
            Dim result As Object
            result = type.InvokeMember("MainFunc", BindingFlags.[Default] Or BindingFlags.InvokeMethod, Nothing, myObj, arguments)
            If Not result Is Nothing Then parsefile(result.toarray)
        End If
    End Sub
    Sub prase(ByVal z As String, ByVal source As String)
        If ADMS.Visible = False Then ADMS.Visible = True
        ADMS.DropDownItems.Add(z)
        addonlist.Add(z & "!@!" & source)
        wl("Addon loaded: " & z)
    End Sub
    Private Sub AdMS_DropDownItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles ADMS.DropDownItemClicked
        For Each itemn As String In addonlist
            If itemn.Contains(e.ClickedItem.Text) Then
                Dim array As String() = itemn.Split("!@!".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                genericparser(array(1), 2)
            End If
        Next
    End Sub
#End Region
    Private Sub MainF_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        searchaddons()
        If Not ostart = "" Then openfile(ostart) Else If Not My.Settings.defaultfile = "" Then openfile(My.Settings.defaultfile)
    End Sub

    Private Sub ToolStripMenuItem3_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem3.Click
        closerr()
    End Sub

    Private Sub ToolStripMenuItem4_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem4.Click
        jcount = 0
        acclist.Clear()
        acclistdet.Clear()
        field.Controls.Clear()
        resetarrays()
        filepath = ""
        filep = ""
        l_pro.Text = "New Project*"
        mtb.SelectedIndex = 0
        wl("New project")
    End Sub

    Private Sub field_DragEnter(sender As Object, e As DragEventArgs) Handles field.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        End If
    End Sub

    Private Sub field_DragDrop(sender As Object, e As DragEventArgs) Handles field.DragDrop
        Dim files() As String = e.Data.GetData(DataFormats.FileDrop)
        openfile(files(0))
    End Sub

    Private Sub mtb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles mtb.SelectedIndexChanged
        If mtb.SelectedIndex = 1 Then
            exportledger2(1)
            t_q.Clear()
            wl("Export to Ledger.")
        ElseIf mtb.SelectedIndex = 2 Then
            exporttbal2()
            updata()
            wl("Export to Trial Balance.")
        ElseIf mtb.SelectedIndex = 3 Then
            exportis2()
            updata()
            wl("Export to Income Statement.")
        ElseIf mtb.SelectedIndex = 4 Then
            exportbalsheet2(True, True, True)
            updata()
            wl("Export to Balance Sheet.")
        End If
        Me.Text = "Ivan's Accounting Tools - " & mtb.SelectedTab.Text
    End Sub
    Sub updata()
        tb_name.Location = New Point(((tbal_p.Width - tb_name.Width) / 2), (tbal_p.Height / 2))
        tb_date.Location = New Point(((tbal_p.Width - tb_date.Width) / 2), (tbal_p.Height / 2 - tb_date.Height))
        is_name.Location = New Point(((is_p.Width - is_name.Width) / 2), (is_p.Height / 2))
        is_date.Location = New Point(((is_p.Width - is_date.Width) / 2), (is_p.Height / 2 - is_date.Height))
        bs_name.Location = New Point(((bs_p.Width - bs_name.Width) / 2), (bs_p.Height / 2))
        bs_date.Location = New Point(((bs_p.Width - bs_date.Width) / 2), (bs_p.Height / 2 - bs_date.Height))
        If dtype = 0 Then
            Dim zdate As Date = FormatDateTime(New Date(Date.Today.Year, 12, DateTime.DaysInMonth(Date.Today.Year, 12)), DateFormat.LongDate)
            tb_name.Text = "Trial Balance as of " & Date.Today.ToLongDateString
            is_name.Text = "Income Statement for the year ended " & zdate.ToLongDateString
            bs_name.Text = "Balance Sheet as of " & Date.Today.ToLongDateString
        ElseIf dtype = 1 Then
            tb_name.Text = "Trial Balance as of " & cudate.ToLongDateString
            is_name.Text = "Income Statement for the year ended " & edate.ToLongDateString
            bs_name.Text = "Balance Sheet as of " & cudate.ToLongDateString
        End If
        If bname = "" Then
            tb_date.Text = "Business Name"
            is_date.Text = "Business Name"
            bs_date.Text = "Business Name"
        Else
            tb_date.Text = bname
            is_date.Text = bname
            bs_date.Text = bname
        End If

    End Sub

    Private Sub t_q_TextChanged(sender As Object, e As EventArgs) Handles t_q.TextChanged
        If Not t_q.Text = "" Then
            For Each myuc As tricolledger In l_field.Controls
                myuc.Visible = False
                If myuc.Name.ToUpper.Contains(t_q.Text.ToUpper) Then
                    myuc.Visible = True
                End If
            Next
        Else
            For Each myuc As tricolledger In l_field.Controls
                myuc.Visible = True
            Next
        End If
    End Sub

    Private Sub MainF_SizeChanged(sender As Object, e As EventArgs) Handles MyBase.SizeChanged
        updata()
    End Sub

    Private Sub ToolStripMenuItem5_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem5.Click
        medit.Show()
    End Sub

    Private Sub ToolStripMenuItem6_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem6.Click
        If Not l_pro.Text = "New Project*" Then
            savetext = ""
            For Each azz As String In acclistdet.Cast(Of String)()
                wst("<AC>" & azz)
            Next
            For i As Integer = 0 To jcount
                For Each sn As journals In field.Controls.OfType(Of journals)
                    If sn.Name = i.ToString("D4") Then savetext = savetext & vbNewLine & sn.dumpcode()
                Next
            Next
            wst("<BN>" & bname)
            wst("<DT>" & dtype)
            wst("<CD>" & cudate)
            wst("<ED>" & edate)
            For Each zee As String In macro
                wst(zee)
            Next
            File.WriteAllText(filep, savetext)
        Else
            stsm.PerformClick()
        End If
    End Sub
    Function checkchanges() As Boolean
        savetext = ""
        For Each azz As String In acclistdet.Cast(Of String)()
            wst("<AC>" & azz)
        Next
        For i As Integer = 0 To jcount
            For Each sn As journals In field.Controls.OfType(Of journals)
                If sn.Name = i.ToString("D4") Then savetext = savetext & vbNewLine & sn.dumpcode()
            Next
        Next
        wst("<BN>" & bname)
        wst("<DT>" & dtype)
        wst("<CD>" & cudate)
        wst("<ED>" & edate)
        For Each zee As String In macro
            wst(zee)
        Next
        If savetext.Contains("<J>") Then
            If Not l_pro.Text = "New Project*" Then
                If Not savetext = File.ReadAllText(filep) Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return True
            End If
        Else
            Return False
        End If
        savetext = ""
    End Function

    Private Sub gj_q_TextChanged(sender As Object, e As EventArgs) Handles gj_q.TextChanged
        If Not gj_q.Text = "" Then
            For Each myuc As journals In field.Controls
                myuc.Visible = False
                If myuc.Name.ToUpper.Contains(gj_q.Text.ToUpper) Then
                    myuc.Visible = True
                End If
            Next
        Else
            For Each myuc As journals In field.Controls
                myuc.Visible = True
            Next
        End If
    End Sub

    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton1.Click
        tsae.PerformClick()
    End Sub

    Private Sub ExportToExcelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExportToExcelToolStripMenuItem.Click
        eexcel(mtb.SelectedIndex)
    End Sub

    Private Sub ConsoleToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsoleToolStripMenuItem.Click
        consolas.Show()
    End Sub
    Sub webreport(ByVal z As Integer)
        exportledger2(1)
        wl("Export to Ledger.")

        exporttbal2()

        wl("Export to Trial Balance.")

        exportis2()

        wl("Export to Income Statement.")

        exportbalsheet2(True, True, True)

        wl("Export to Balance Sheet.")
        updata()

        wl("Prepare Web Report.")
        If z = 1 Then
            Dim zz As String = My.Resources.template
            Dim zz2 As String
            zz2 = zz.Replace("%%title%%", "IVACT Web Report")
            zz = zz2.Replace("%%header1%%", bname)
            zz2 = zz.Replace("%%header2%%", "General Journal")
            Dim zz3 As String
            Dim th As New ArrayList
            th.Add("<table class=""table"">")
            th.Add("<tr>")
            th.Add("<th>Date</th>")
            th.Add("<th>Accounts and Explanation</th>")
            th.Add("<th>Debit</th>")
            th.Add("<th>Credit</th>")
            th.Add("</tr>")
            Dim ctn As New ArrayList
            For i As Integer = 0 To jcount
                For Each myuc As journals In field.Controls.OfType(Of journals)
                    If myuc.Name = i.ToString("D4") Then
                        ctn.Add("<p class=""text-left"">Journal No: " & myuc.Name & "</p>")
                        ctn.AddRange(th)
                        For j = 0 To myuc.dg.Rows.Count - 1
                            ctn.Add("<tr class=""text-left"">")
                            ctn.Add("<td>" & myuc.dg.Rows(j).Cells(0).Value & "</td>")
                            ctn.Add("<td>" & myuc.dg.Rows(j).Cells(1).Value & "</td>")
                            ctn.Add("<td>" & myuc.dg.Rows(j).Cells(2).Value & "</td>")
                            ctn.Add("<td>" & myuc.dg.Rows(j).Cells(3).Value & "</td>")
                            ctn.Add("</tr>")
                        Next
                    End If
                Next
                ctn.Add("</table>")
                ctn.Add("<hr>")
            Next

            For Each azz As String In ctn.Cast(Of String)()
                zz3 = zz3 & vbNewLine & azz
            Next
            zz = zz2.Replace("%%content%%", zz3)
            zz2 = zz
            File.WriteAllText("generaljournal.html", zz2)
        ElseIf z = 2 Then
            Dim zz As String = My.Resources.template
            Dim zz2 As String
            zz2 = zz.Replace("%%title%%", "IVACT Web Report")
            zz = zz2.Replace("%%header1%%", bname)
            zz2 = zz.Replace("%%header2%%", "Ledger")
            Dim zz3 As String
            Dim th As New ArrayList
            th.Add("<table class=""table"">")
            th.Add("<tr>")
            th.Add("<th>Date</th>")
            th.Add("<th>Accounts and Explanation</th>")
            th.Add("<th>Reference</th>")
            th.Add("<th>Debit</th>")
            th.Add("<th>Credit</th>")
            th.Add("<th>Balance</th>")
            th.Add("</tr>")
            Dim ctn As New ArrayList
            For Each myuc As tricolledger In l_field.Controls.OfType(Of tricolledger)
                ctn.Add("<p class=""text-left"">" & myuc.Name & "</p>")
                ctn.AddRange(th)
                For je = 0 To myuc.dg.Rows.Count - 1
                    ctn.Add("<tr class=""text-left"">")
                    ctn.Add("<td>" & myuc.dg.Rows(je).Cells(0).Value & "</td>")
                    ctn.Add("<td>" & myuc.dg.Rows(je).Cells(1).Value & "</td>")
                    ctn.Add("<td>" & myuc.dg.Rows(je).Cells(2).Value & "</td>")
                    ctn.Add("<td>" & myuc.dg.Rows(je).Cells(3).Value & "</td>")
                    ctn.Add("<td>" & myuc.dg.Rows(je).Cells(4).Value & "</td>")
                    ctn.Add("<td>" & myuc.dg.Rows(je).Cells(5).Value & "</td>")
                    ctn.Add("</tr>")
                Next
                ctn.Add("</table>")
                ctn.Add("<hr>")
            Next
            For Each azz As String In ctn.Cast(Of String)()
                zz3 = zz3 & vbNewLine & azz
            Next
            zz = zz2.Replace("%%content%%", zz3)
            zz2 = zz
            File.WriteAllText("ledger.html", zz2)
        ElseIf z = 3 Then
            Dim zz As String = My.Resources.template
            Dim zz2 As String
            zz2 = zz.Replace("%%title%%", "IVACT Web Report")
            zz = zz2.Replace("%%header1%%", tb_date.Text)
            zz2 = zz.Replace("%%header2%%", tb_name.Text)
            Dim zz3 As String
            Dim th As New ArrayList
            th.Add("<table class=""table"">")
            th.Add("<tr>")
            th.Add("<th>Account</th>")
            th.Add("<th>Debit</th>")
            th.Add("<th>Credit</th>")
            th.Add("</tr>")
            Dim ctn As New ArrayList
            ctn.AddRange(th)
            For je = 0 To tbal_dg.Rows.Count - 1
                ctn.Add("<tr class=""text-left"">")
                ctn.Add("<td>" & tbal_dg.Rows(je).Cells(0).Value & "</td>")
                ctn.Add("<td>" & tbal_dg.Rows(je).Cells(1).Value & "</td>")
                ctn.Add("<td>" & tbal_dg.Rows(je).Cells(2).Value & "</td>")
                ctn.Add("</tr>")
            Next
            ctn.Add("</table>")
            For Each azz As String In ctn.Cast(Of String)()
                zz3 = zz3 & vbNewLine & azz
            Next
            zz = zz2.Replace("%%content%%", zz3)
            zz2 = zz
            File.WriteAllText("trialbalance.html", zz2)
        ElseIf z = 4 Then
            Dim zz As String = My.Resources.template
            Dim zz2 As String
            zz2 = zz.Replace("%%title%%", "IVACT Web Report")
            zz = zz2.Replace("%%header1%%", is_date.Text)
            zz2 = zz.Replace("%%header2%%", is_name.Text)
            Dim zz3 As String
            Dim th As New ArrayList
            th.Add("<table class=""table"">")
            Dim ctn As New ArrayList
            ctn.AddRange(th)
            For je = 0 To is_dg.Rows.Count - 1
                If is_dg.Rows(je).Cells(0).Value = "" And is_dg.Rows(je).Cells(1).Value = "" And is_dg.Rows(je).Cells(2).Value = "" And is_dg.Rows(je).Cells(3).Value = "" Then
                Else
                    ctn.Add("<tr class=""text-left"">")
                    ctn.Add("<td>" & is_dg.Rows(je).Cells(0).Value & "</td>")
                    ctn.Add("<td>" & is_dg.Rows(je).Cells(1).Value & "</td>")
                    ctn.Add("<td>" & is_dg.Rows(je).Cells(2).Value & "</td>")
                    ctn.Add("<td>" & is_dg.Rows(je).Cells(3).Value & "</td>")
                    ctn.Add("</tr>")
                End If
            Next
            ctn.Add("</table>")
            For Each azz As String In ctn.Cast(Of String)()
                zz3 = zz3 & vbNewLine & azz
            Next
            zz = zz2.Replace("%%content%%", zz3)
            zz2 = zz
            File.WriteAllText("incomestatement.html", zz2)
        ElseIf z = 5 Then
            Dim zz As String = My.Resources.template
            Dim zz2 As String
            zz2 = zz.Replace("%%title%%", "IVACT Web Report")
            zz = zz2.Replace("%%header1%%", bs_date.Text)
            zz2 = zz.Replace("%%header2%%", bs_name.Text)
            Dim zz3 As String
            Dim th As New ArrayList
            th.Add("<table class=""table"">")
            Dim ctn As New ArrayList
            ctn.AddRange(th)
            For je = 0 To dg.Rows.Count - 1
                If dg.Rows(je).Cells(0).Value = "" And dg.Rows(je).Cells(1).Value = "" And dg.Rows(je).Cells(2).Value = "" And dg.Rows(je).Cells(3).Value = "" Then
                Else
                    ctn.Add("<tr class=""text-left"">")
                    ctn.Add("<td>" & dg.Rows(je).Cells(0).Value & "</td>")
                    ctn.Add("<td>" & dg.Rows(je).Cells(1).Value & "</td>")
                    ctn.Add("<td>" & dg.Rows(je).Cells(2).Value & "</td>")
                    ctn.Add("<td>" & dg.Rows(je).Cells(3).Value & "</td>")
                    ctn.Add("</tr>")
                End If
            Next
            ctn.Add("</table>")
            For Each azz As String In ctn.Cast(Of String)()
                zz3 = zz3 & vbNewLine & azz
            Next
            zz = zz2.Replace("%%content%%", zz3)
            zz2 = zz
            File.WriteAllText("balancesheet.html", zz2)
        End If
    End Sub
    Private Sub html_exp_Click(sender As Object, e As EventArgs) Handles html_exp.Click
        webreport(1)
        webreport(2)
        webreport(3)
        webreport(4)
        webreport(5)
    End Sub

    Private Sub ts_mac_DropDownItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles ts_mac.DropDownItemClicked
        If Not e.ClickedItem.Name = tscm.Name Then
            parsefile(returnmacro(e.ClickedItem.Text, 1).ToArray)
        End If
    End Sub
    Public Sub refreshmacros()
        If ts_mac.DropDownItems.Count > 2 Then
            Dim zaaa As ToolStripDropDownItem = ts_mac.DropDownItems(0)
            Dim zaaa2 As ToolStripSeparator = ts_mac.DropDownItems(1)
            ts_mac.DropDownItems.Clear()
            ts_mac.DropDownItems.Add(zaaa)
            ts_mac.DropDownItems.Add(zaaa2)
        End If
        For Each za As String In macro
            If za.Contains("<M>") Then
                ts_mac.DropDownItems.Add(za.Replace("<M>", ""))
                If ts_sm.Visible = False Then ts_sm.Visible = True
            End If
        Next
    End Sub
    Function returnmacro(ByVal z As String, ByVal mode As Integer) As ArrayList
        Dim mcl As New ArrayList
        Dim lint As Integer = -1
        Dim ze As String
        For i = 0 To macro.Count - 1
            ze = macro(i)
            If ze = "<M>" & z Then
                lint = i
                If mode = 1 Then mcl.Add("<J>" & jcount.ToString("D4")) Else mcl.Add(ze)
            End If
            If i > lint And Not lint < 0 And ze.Contains("<MD>") Then
                If mode = 1 Then mcl.Add(ze.Replace("<MD>", "<D>").Replace("%DATE%", Date.Today.ToString("dd MMMM yyyy"))) Else mcl.Add(ze)
            End If
            If i > lint And Not lint < 0 And ze.Contains("<MC>") Then
                If mode = 1 Then mcl.Add(ze.Replace("<MC>", "<C>").Replace("%DATE%", Date.Today.ToString("dd MMMM yyyy"))) Else mcl.Add(ze)
            End If
            If i > lint And Not lint < 0 And ze.Contains("<MDE>") Then
                If mode = 1 Then mcl.Add(ze.Replace("<MDE>", "<DE>").Replace("%MONTH%", Date.Today.ToString("MMMM"))) Else mcl.Add(ze)
            End If
            If i > lint And Not lint < 0 And ze.Contains("</M>") Then
                If mode = 1 Then mcl.Add("</J>") Else mcl.Add(ze)
                Return mcl
                Exit Function
            End If
        Next
    End Function

    Private Sub tscm_Click(sender As Object, e As EventArgs) Handles tscm.Click
        macroeditor.Show()
    End Sub

    Private Sub SettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SettingsToolStripMenuItem.Click
        settings.Show()
    End Sub

    Private Sub RestartStripMenuItem1_Click(sender As Object, e As EventArgs) Handles RestartStripMenuItem1.Click
        If checkchanges() = False Then
            Application.Restart()
        Else
            Dim dlg As New changedetect
            If dlg.ShowDialog = MsgBoxResult.Ok Then
                Application.Restart()
            ElseIf MsgBoxResult.Yes Then
                ToolStripMenuItem6.PerformClick()
                Application.Restart()
            ElseIf MsgBoxResult.Cancel Then
                Exit Sub
            End If
        End If
    End Sub

    Private Sub MainF_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        ExitToolStripMenuItem.PerformClick()
    End Sub

    Private Sub t_netw_Tick(sender As Object, e As EventArgs) Handles t_netw.Tick
        If Listener.Pending = True Then
            Message = ""
            Client = Listener.AcceptTcpClient()
            Dim Reader As New StreamReader(Client.GetStream())
            While Reader.Peek > -1
                    Message = Message + Convert.ToChar(Reader.Read()).ToString
                End While
            procmsg(Message)
        End If
    End Sub
End Class
