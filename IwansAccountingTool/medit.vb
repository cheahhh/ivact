﻿Public Class medit
    Private Sub cusdate_CheckedChanged(sender As Object, e As EventArgs) Handles cusdate.CheckedChanged
        If cusdate.Checked = True Then
            ml1.Enabled = True
            ml2.Enabled = True
            edate.Enabled = True
            cudate.Enabled = True
        Else
            ml1.Enabled = False
            ml2.Enabled = False
            edate.Enabled = False
            cudate.Enabled = False
        End If
    End Sub

    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton1.Click
        MainF.bname = bname.Text
        If stdate.Checked = True Then
            MainF.dtype = 0
        Else
            MainF.dtype = 1
            MainF.edate = FormatDateTime(edate.Value.Date, DateFormat.LongDate)
            MainF.cudate = FormatDateTime(cudate.Value.Date, DateFormat.LongDate)
        End If
        Me.Close()
    End Sub

    Private Sub medit_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bname.Text = MainF.bname
        If MainF.dtype = 0 Then
            stdate.Checked = True
        Else
            cusdate.Checked = True
            edate.Value = MainF.edate
            cudate.Value = MainF.cudate
        End If
        ml1.Select()
    End Sub
End Class