﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pbs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CheckBox1 = New MaterialSkin.Controls.MaterialCheckBox()
        Me.CheckBox2 = New MaterialSkin.Controls.MaterialCheckBox()
        Me.CheckBox3 = New MaterialSkin.Controls.MaterialCheckBox()
        Me.t_re = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.MaterialLabel1 = New MaterialSkin.Controls.MaterialLabel()
        Me.MaterialRaisedButton1 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.SuspendLayout()
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Depth = 0
        Me.CheckBox1.Font = New System.Drawing.Font("Roboto", 10.0!)
        Me.CheckBox1.Location = New System.Drawing.Point(9, 9)
        Me.CheckBox1.Margin = New System.Windows.Forms.Padding(0)
        Me.CheckBox1.MouseLocation = New System.Drawing.Point(-1, -1)
        Me.CheckBox1.MouseState = MaterialSkin.MouseState.HOVER
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Ripple = True
        Me.CheckBox1.Size = New System.Drawing.Size(72, 30)
        Me.CheckBox1.TabIndex = 7
        Me.CheckBox1.Text = "Assets"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Depth = 0
        Me.CheckBox2.Font = New System.Drawing.Font("Roboto", 10.0!)
        Me.CheckBox2.Location = New System.Drawing.Point(9, 39)
        Me.CheckBox2.Margin = New System.Windows.Forms.Padding(0)
        Me.CheckBox2.MouseLocation = New System.Drawing.Point(-1, -1)
        Me.CheckBox2.MouseState = MaterialSkin.MouseState.HOVER
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Ripple = True
        Me.CheckBox2.Size = New System.Drawing.Size(89, 30)
        Me.CheckBox2.TabIndex = 8
        Me.CheckBox2.Text = "Liabilities"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Depth = 0
        Me.CheckBox3.Font = New System.Drawing.Font("Roboto", 10.0!)
        Me.CheckBox3.Location = New System.Drawing.Point(9, 69)
        Me.CheckBox3.Margin = New System.Windows.Forms.Padding(0)
        Me.CheckBox3.MouseLocation = New System.Drawing.Point(-1, -1)
        Me.CheckBox3.MouseState = MaterialSkin.MouseState.HOVER
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Ripple = True
        Me.CheckBox3.Size = New System.Drawing.Size(120, 30)
        Me.CheckBox3.TabIndex = 9
        Me.CheckBox3.Text = "Owner's Equity"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        't_re
        '
        Me.t_re.Depth = 0
        Me.t_re.Hint = "Override Retained Earnings"
        Me.t_re.Location = New System.Drawing.Point(87, 102)
        Me.t_re.MaxLength = 32767
        Me.t_re.MouseState = MaterialSkin.MouseState.HOVER
        Me.t_re.Name = "t_re"
        Me.t_re.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.t_re.SelectedText = ""
        Me.t_re.SelectionLength = 0
        Me.t_re.SelectionStart = 0
        Me.t_re.Size = New System.Drawing.Size(184, 23)
        Me.t_re.TabIndex = 11
        Me.t_re.TabStop = False
        Me.t_re.UseSystemPasswordChar = False
        '
        'MaterialLabel1
        '
        Me.MaterialLabel1.AutoSize = True
        Me.MaterialLabel1.Depth = 0
        Me.MaterialLabel1.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.MaterialLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialLabel1.Location = New System.Drawing.Point(12, 102)
        Me.MaterialLabel1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialLabel1.Name = "MaterialLabel1"
        Me.MaterialLabel1.Size = New System.Drawing.Size(69, 19)
        Me.MaterialLabel1.TabIndex = 12
        Me.MaterialLabel1.Text = "Optional:"
        '
        'MaterialRaisedButton1
        '
        Me.MaterialRaisedButton1.Depth = 0
        Me.MaterialRaisedButton1.Location = New System.Drawing.Point(196, 131)
        Me.MaterialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton1.Name = "MaterialRaisedButton1"
        Me.MaterialRaisedButton1.Primary = True
        Me.MaterialRaisedButton1.Size = New System.Drawing.Size(75, 23)
        Me.MaterialRaisedButton1.TabIndex = 13
        Me.MaterialRaisedButton1.Text = "Export"
        Me.MaterialRaisedButton1.UseVisualStyleBackColor = True
        '
        'pbs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(285, 161)
        Me.Controls.Add(Me.MaterialRaisedButton1)
        Me.Controls.Add(Me.MaterialLabel1)
        Me.Controls.Add(Me.t_re)
        Me.Controls.Add(Me.CheckBox3)
        Me.Controls.Add(Me.CheckBox2)
        Me.Controls.Add(Me.CheckBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "pbs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Partial Balance Sheet"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CheckBox1 As MaterialSkin.Controls.MaterialCheckBox
    Friend WithEvents CheckBox2 As MaterialSkin.Controls.MaterialCheckBox
    Friend WithEvents CheckBox3 As MaterialSkin.Controls.MaterialCheckBox
    Friend WithEvents t_re As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents MaterialLabel1 As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents MaterialRaisedButton1 As MaterialSkin.Controls.MaterialRaisedButton
End Class
