﻿Imports System.IO

Public Class balsheet
    Public savetext As String = "[BALANCESHEET]"
    Private Sub tBal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        updata()
    End Sub
    Sub updata()
        b_name.Location = New Point(((p_ban.Width - b_name.Width) / 2), (p_ban.Height / 2))
        b_date.Location = New Point(((p_ban.Width - b_date.Width) / 2), (p_ban.Height / 2 - b_date.Height))
    End Sub

    Private Sub TBalance_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.SizeChanged
        updata()
    End Sub

    Private Sub TBalance_LocationChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.LocationChanged
        updata()
    End Sub

    Private Sub c_name_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles c_name.TextChanged
        b_date.Text = c_name.Text
        updata()
    End Sub

    Private Sub c_dat_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles c_dat.TextChanged
        b_name.Text = c_dat.Text
        updata()
    End Sub
    Sub wstate(ByVal s As String)
        If savetext = "" Then
            savetext = s
        Else
            savetext = savetext & vbNewLine & s
        End If
    End Sub

    Private Sub b_exp_Click(sender As Object, e As EventArgs) Handles b_exp.Click
        Dim dlg As New SaveFileDialog
        dlg.Filter = "IVACT (*.ivact)|*.ivact"
        dlg.Title = "Save Balance Sheet Output"
        If dlg.ShowDialog = DialogResult.OK Then
            savetext = "[BALANCESHEET]"
            wstate("<H1>" & c_name.Text)
            wstate("<H2>" & c_dat.Text)
            For i = 0 To dg.Rows.Count - 1
                wstate("<LI>" & dg.Rows(i).Cells(0).Value & "/" & dg.Rows(i).Cells(1).Value & "/" & dg.Rows(i).Cells(2).Value & "/" & dg.Rows(i).Cells(3).Value)
            Next
            File.WriteAllText(dlg.FileName, savetext)
        End If
    End Sub
End Class