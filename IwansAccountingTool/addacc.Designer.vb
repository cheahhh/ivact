﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class addacc
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbt = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tname = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.tbal = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tt = New System.Windows.Forms.ToolTip(Me.components)
        Me.k_open = New System.Windows.Forms.CheckBox()
        Me.k_is = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'cbt
        '
        Me.cbt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbt.FormattingEnabled = True
        Me.cbt.Items.AddRange(New Object() {"Revenue (Main)", "Revenue (Side)", "Contra (Revenue)", "Cost of Goods Sold", "Expenses", "Assets (Non-current)", "Assets (Current)", "Contra (Current Assets)", "Contra (Non-current Assets)", "Liabilities (Non-current)", "Liabilities (Current)", "Contra (Current Liabilities)", "Contra (Non-current Liabilities)", "Owner's Equity (Capital)", "Owner's Equity (Drawing)", "Retained Earnings"})
        Me.cbt.Location = New System.Drawing.Point(52, 32)
        Me.cbt.Name = "cbt"
        Me.cbt.Size = New System.Drawing.Size(255, 21)
        Me.cbt.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Type:"
        Me.tt.SetToolTip(Me.Label1, "Type of the Account")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Name:"
        Me.tt.SetToolTip(Me.Label2, "Name of the Account")
        '
        'tname
        '
        Me.tname.Location = New System.Drawing.Point(52, 6)
        Me.tname.Name = "tname"
        Me.tname.Size = New System.Drawing.Size(255, 20)
        Me.tname.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(212, 85)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(95, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Add Account"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'tbal
        '
        Me.tbal.Location = New System.Drawing.Point(52, 59)
        Me.tbal.Name = "tbal"
        Me.tbal.Size = New System.Drawing.Size(255, 20)
        Me.tbal.TabIndex = 6
        Me.tbal.Text = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(19, 62)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(27, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "B.B:"
        Me.tt.SetToolTip(Me.Label3, "Beginning Balance (Opening Balance) of the Account")
        '
        'tt
        '
        Me.tt.BackColor = System.Drawing.Color.White
        '
        'k_open
        '
        Me.k_open.AutoSize = True
        Me.k_open.Location = New System.Drawing.Point(84, 89)
        Me.k_open.Name = "k_open"
        Me.k_open.Size = New System.Drawing.Size(122, 17)
        Me.k_open.TabIndex = 7
        Me.k_open.Text = "Keep Window Open"
        Me.k_open.UseVisualStyleBackColor = True
        Me.k_open.Visible = False
        '
        'k_is
        '
        Me.k_is.AutoSize = True
        Me.k_is.Checked = True
        Me.k_is.CheckState = System.Windows.Forms.CheckState.Checked
        Me.k_is.Location = New System.Drawing.Point(12, 89)
        Me.k_is.Name = "k_is"
        Me.k_is.Size = New System.Drawing.Size(58, 17)
        Me.k_is.TabIndex = 8
        Me.k_is.Text = "iSense"
        Me.k_is.UseVisualStyleBackColor = True
        '
        'addacc
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(319, 113)
        Me.Controls.Add(Me.k_is)
        Me.Controls.Add(Me.k_open)
        Me.Controls.Add(Me.tbal)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tname)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbt)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "addacc"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add Account"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbt As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents tname As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents tbal As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents tt As ToolTip
    Friend WithEvents k_open As CheckBox
    Friend WithEvents k_is As CheckBox
End Class
