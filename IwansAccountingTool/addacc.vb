﻿Public Class addacc
    Public snd As Integer
    Public mode As Integer = 0
    Public mint As Integer
    Sub def(ByVal ty As String)
        tname.Text = ty
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If tname.Text = "" Then
            MsgBox("Account name cannot be blank", MsgBoxStyle.OkOnly, "Errr...")
            Exit Sub
        End If
        If cbt.Text = "" Then
            MsgBox("Choose an account type", MsgBoxStyle.OkOnly, "Errr...")
            Exit Sub
        End If
        If tbal.Text = "" Then
            MsgBox("Account's opening balance cannot be blank", MsgBoxStyle.OkOnly, "Errr...")
            Exit Sub
        End If
        If mode = 0 Then
            MainF.acclist.Add(tname.Text)
            MainF.acclistdet.Add(tname.Text & "/" & cbt.Text & "/" & tbal.Text)
            If MainF.t_netw.Enabled = True Then addnewaccount("<AC>" & tname.Text & "/" & cbt.Text & "/" & tbal.Text)
            AddJ.refreshac()
            If snd = 1 Then AddJ.Button1.PerformClick()
            If snd = 2 Then AddJ.Button2.PerformClick()
            Me.Close()
        End If
        If mode = 1 Then
            manAcc.dg.Rows(mint).Cells(0).Value = tname.Text
            manAcc.dg.Rows(mint).Cells(1).Value = cbt.Text
            manAcc.dg.Rows(mint).Cells(2).Value = tbal.Text
            Me.Close()
        End If
        If mode = 2 Then
            For i = 0 To manAcc.dg.Rows.Count - 1
                If manAcc.dg.Rows(i).Cells(0).Value = tname.Text Then
                    MsgBox("Account already exists", MsgBoxStyle.OkCancel, "Errr...")
                    Exit Sub
                End If
            Next
            manAcc.dg.Rows.Add(New String() {tname.Text, cbt.Text, tbal.Text})
            If Not k_open.Checked = True Then
                Me.Close()
            Else
                tname.Clear()
                tbal.Clear()
                tname.Focus()
            End If
        End If
    End Sub

    Private Sub tbal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbal.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            Button1.PerformClick()
        End If
    End Sub

    Private Sub addacc_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
    Sub isense()
        If tname.Text.Contains("Expenses") Or tname.Text.Contains("Expense") Then
            If Not tname.Text.Contains("Prepaid") Then
                If Not tname.Text.Contains("Accrued") Then
                    cbt.Text = "Expenses"
                Else cbt.Text = "Liabilities (Current)"
                End If
            Else cbt.Text = "Assets (Current)"
            End If
        ElseIf tname.Text.Contains("Revenue") Then
            If Not tname.Text.Contains("Unearned") Then
                cbt.Text = "Revenue (Main)"
            Else
                cbt.Text = "Liabilities (Current)"
            End If
        ElseIf tname.Text.Contains("Accounts Receivable") Or tname.Text.Contains("Cash") Then
            cbt.Text = "Assets (Current)"
        ElseIf tname.Text.Contains("Accounts Payable") Then
            cbt.Text = "Liabilities (Current)"
        ElseIf tname.Text.Contains("Bonds Payable") Then
            cbt.Text = "Liabilities (Non-current)"
        ElseIf tname.Text.Contains("Accumulated Depreciation") Then
            cbt.Text = "Contra (Non-current Assets)"
        ElseIf tname.Text.Contains("Cost of Goods Sold") Then
            cbt.Text = "Cost of Goods Sold"
        End If
    End Sub

    Private Sub tname_TextChanged(sender As Object, e As EventArgs) Handles tname.TextChanged
        If k_is.Checked = True Then isense()
    End Sub
End Class