﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class tricolledger
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dg = New System.Windows.Forms.DataGridView()
        Me.ddate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.accs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.deb = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cred = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LName = New System.Windows.Forms.Label()
        CType(Me.dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dg
        '
        Me.dg.AllowUserToAddRows = False
        Me.dg.AllowUserToDeleteRows = False
        Me.dg.AllowUserToResizeColumns = False
        Me.dg.AllowUserToResizeRows = False
        Me.dg.BackgroundColor = System.Drawing.Color.White
        Me.dg.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ddate, Me.accs, Me.jno, Me.deb, Me.cred, Me.bal})
        Me.dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dg.GridColor = System.Drawing.Color.White
        Me.dg.Location = New System.Drawing.Point(0, 13)
        Me.dg.Name = "dg"
        Me.dg.ReadOnly = True
        Me.dg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg.Size = New System.Drawing.Size(615, 112)
        Me.dg.TabIndex = 3
        '
        'ddate
        '
        Me.ddate.FillWeight = 115.0!
        Me.ddate.HeaderText = "Date"
        Me.ddate.Name = "ddate"
        Me.ddate.ReadOnly = True
        Me.ddate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ddate.Width = 115
        '
        'accs
        '
        Me.accs.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.accs.HeaderText = "Accounts (Explanation)"
        Me.accs.Name = "accs"
        Me.accs.ReadOnly = True
        '
        'jno
        '
        Me.jno.HeaderText = "Ref."
        Me.jno.Name = "jno"
        Me.jno.ReadOnly = True
        Me.jno.Width = 70
        '
        'deb
        '
        Me.deb.HeaderText = "Debit"
        Me.deb.Name = "deb"
        Me.deb.ReadOnly = True
        '
        'cred
        '
        Me.cred.HeaderText = "Credit"
        Me.cred.Name = "cred"
        Me.cred.ReadOnly = True
        '
        'bal
        '
        Me.bal.HeaderText = "Balance"
        Me.bal.Name = "bal"
        Me.bal.ReadOnly = True
        '
        'LName
        '
        Me.LName.AutoSize = True
        Me.LName.Dock = System.Windows.Forms.DockStyle.Top
        Me.LName.Location = New System.Drawing.Point(0, 0)
        Me.LName.Name = "LName"
        Me.LName.Size = New System.Drawing.Size(59, 13)
        Me.LName.TabIndex = 2
        Me.LName.Text = "ACCNAME"
        '
        'tricolledger
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.dg)
        Me.Controls.Add(Me.LName)
        Me.Name = "tricolledger"
        Me.Size = New System.Drawing.Size(615, 125)
        CType(Me.dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dg As DataGridView
    Friend WithEvents LName As Label
    Friend WithEvents ddate As DataGridViewTextBoxColumn
    Friend WithEvents accs As DataGridViewTextBoxColumn
    Friend WithEvents jno As DataGridViewTextBoxColumn
    Friend WithEvents deb As DataGridViewTextBoxColumn
    Friend WithEvents cred As DataGridViewTextBoxColumn
    Friend WithEvents bal As DataGridViewTextBoxColumn
End Class
