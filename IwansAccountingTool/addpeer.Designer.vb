﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class addpeer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.t_ip = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.MaterialRaisedButton1 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.SuspendLayout()
        '
        't_ip
        '
        Me.t_ip.Depth = 0
        Me.t_ip.Hint = "IP Address"
        Me.t_ip.Location = New System.Drawing.Point(12, 12)
        Me.t_ip.MaxLength = 32767
        Me.t_ip.MouseState = MaterialSkin.MouseState.HOVER
        Me.t_ip.Name = "t_ip"
        Me.t_ip.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.t_ip.SelectedText = ""
        Me.t_ip.SelectionLength = 0
        Me.t_ip.SelectionStart = 0
        Me.t_ip.Size = New System.Drawing.Size(239, 23)
        Me.t_ip.TabIndex = 2
        Me.t_ip.TabStop = False
        Me.t_ip.UseSystemPasswordChar = False
        '
        'MaterialRaisedButton1
        '
        Me.MaterialRaisedButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MaterialRaisedButton1.Depth = 0
        Me.MaterialRaisedButton1.Location = New System.Drawing.Point(179, 41)
        Me.MaterialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton1.Name = "MaterialRaisedButton1"
        Me.MaterialRaisedButton1.Primary = True
        Me.MaterialRaisedButton1.Size = New System.Drawing.Size(72, 29)
        Me.MaterialRaisedButton1.TabIndex = 10
        Me.MaterialRaisedButton1.Text = "Add"
        Me.MaterialRaisedButton1.UseVisualStyleBackColor = True
        '
        'addpeer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(263, 78)
        Me.Controls.Add(Me.MaterialRaisedButton1)
        Me.Controls.Add(Me.t_ip)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "addpeer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add Peer"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents t_ip As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents MaterialRaisedButton1 As MaterialSkin.Controls.MaterialRaisedButton
End Class
