﻿Imports System.IO

Public Class TriColLedgerF
    Public savetext As String = "[LEDGER]"
    Private Sub t_q_TextChanged(sender As Object, e As EventArgs) Handles t_q.TextChanged
        If Not t_q.Text = "" Then
            For Each myuc As tricolledger In field.Controls
                myuc.Visible = False
                If myuc.Name.ToUpper.Contains(t_q.Text.ToUpper) Then
                    myuc.Visible = True
                End If
            Next
        Else
            For Each myuc As tricolledger In field.Controls
                myuc.Visible = True
            Next
        End If
    End Sub

    Private Sub b_exp_Click(sender As Object, e As EventArgs) Handles b_exp.Click
        Dim dlg As New SaveFileDialog
        dlg.Filter = "IVACT (*.ivact)|*.ivact"
        dlg.Title = "Save Ledger Output"
        If dlg.ShowDialog = DialogResult.OK Then
            savetext = "[LEDGER]"
            For Each myuc As tricolledger In field.Controls
                myuc.dumpcode()
            Next
            File.WriteAllText(dlg.FileName, savetext)
        End If
    End Sub

    Private Sub t_q_Click(sender As Object, e As EventArgs) Handles t_q.Click

    End Sub
End Class