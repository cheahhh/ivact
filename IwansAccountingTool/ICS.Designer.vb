﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ICS
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ICS))
        Me.p_ban = New System.Windows.Forms.Panel()
        Me.b_date = New System.Windows.Forms.Label()
        Me.b_name = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.c_name = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel()
        Me.c_dat = New System.Windows.Forms.ToolStripTextBox()
        Me.dg = New System.Windows.Forms.DataGridView()
        Me.accs_d = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.amt_d = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Credit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.col3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.b_exp = New System.Windows.Forms.ToolStripButton()
        Me.p_ban.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'p_ban
        '
        Me.p_ban.BackColor = System.Drawing.Color.White
        Me.p_ban.Controls.Add(Me.b_date)
        Me.p_ban.Controls.Add(Me.b_name)
        Me.p_ban.Dock = System.Windows.Forms.DockStyle.Top
        Me.p_ban.Location = New System.Drawing.Point(0, 25)
        Me.p_ban.Name = "p_ban"
        Me.p_ban.Size = New System.Drawing.Size(732, 59)
        Me.p_ban.TabIndex = 5
        '
        'b_date
        '
        Me.b_date.AutoSize = True
        Me.b_date.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.b_date.Location = New System.Drawing.Point(189, 32)
        Me.b_date.Name = "b_date"
        Me.b_date.Size = New System.Drawing.Size(43, 20)
        Me.b_date.TabIndex = 1
        Me.b_date.Text = "Title"
        '
        'b_name
        '
        Me.b_name.AutoSize = True
        Me.b_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.b_name.Location = New System.Drawing.Point(188, 12)
        Me.b_name.Name = "b_name"
        Me.b_name.Size = New System.Drawing.Size(40, 18)
        Me.b_name.TabIndex = 0
        Me.b_name.Text = "Title"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSeparator1, Me.ToolStripLabel1, Me.c_name, Me.ToolStripSeparator2, Me.ToolStripLabel2, Me.c_dat, Me.b_exp})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(732, 25)
        Me.ToolStrip1.TabIndex = 4
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(45, 22)
        Me.ToolStripLabel1.Text = "Name :"
        '
        'c_name
        '
        Me.c_name.Name = "c_name"
        Me.c_name.Size = New System.Drawing.Size(100, 25)
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(38, 22)
        Me.ToolStripLabel2.Text = "Misc :"
        '
        'c_dat
        '
        Me.c_dat.Name = "c_dat"
        Me.c_dat.Size = New System.Drawing.Size(100, 25)
        '
        'dg
        '
        Me.dg.AllowUserToAddRows = False
        Me.dg.AllowUserToDeleteRows = False
        Me.dg.AllowUserToResizeColumns = False
        Me.dg.AllowUserToResizeRows = False
        Me.dg.BackgroundColor = System.Drawing.Color.White
        Me.dg.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.accs_d, Me.amt_d, Me.Credit, Me.col3})
        Me.dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dg.GridColor = System.Drawing.Color.White
        Me.dg.Location = New System.Drawing.Point(0, 84)
        Me.dg.Name = "dg"
        Me.dg.ReadOnly = True
        Me.dg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg.Size = New System.Drawing.Size(732, 459)
        Me.dg.TabIndex = 10
        '
        'accs_d
        '
        Me.accs_d.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.accs_d.HeaderText = ""
        Me.accs_d.Name = "accs_d"
        Me.accs_d.ReadOnly = True
        '
        'amt_d
        '
        Me.amt_d.HeaderText = ""
        Me.amt_d.Name = "amt_d"
        Me.amt_d.ReadOnly = True
        '
        'Credit
        '
        Me.Credit.HeaderText = ""
        Me.Credit.Name = "Credit"
        Me.Credit.ReadOnly = True
        '
        'col3
        '
        Me.col3.HeaderText = ""
        Me.col3.Name = "col3"
        Me.col3.ReadOnly = True
        '
        'b_exp
        '
        Me.b_exp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.b_exp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.b_exp.Image = CType(resources.GetObject("b_exp.Image"), System.Drawing.Image)
        Me.b_exp.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.b_exp.Name = "b_exp"
        Me.b_exp.Size = New System.Drawing.Size(44, 22)
        Me.b_exp.Text = "Export"
        '
        'ICS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(732, 543)
        Me.Controls.Add(Me.dg)
        Me.Controls.Add(Me.p_ban)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "ICS"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Income Statement"
        Me.p_ban.ResumeLayout(False)
        Me.p_ban.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents p_ban As Panel
    Friend WithEvents b_date As Label
    Friend WithEvents b_name As Label
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ToolStripLabel1 As ToolStripLabel
    Friend WithEvents c_name As ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ToolStripLabel2 As ToolStripLabel
    Friend WithEvents c_dat As ToolStripTextBox
    Friend WithEvents dg As DataGridView
    Friend WithEvents accs_d As DataGridViewTextBoxColumn
    Friend WithEvents amt_d As DataGridViewTextBoxColumn
    Friend WithEvents Credit As DataGridViewTextBoxColumn
    Friend WithEvents col3 As DataGridViewTextBoxColumn
    Friend WithEvents b_exp As ToolStripButton
End Class
