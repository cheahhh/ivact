﻿Public Class macroeditor
    Dim onamemacro As String
    Dim tempmacro As New ArrayList
    Private Sub macroeditor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For Each ze As ToolStripDropDownItem In MainF.ts_mac.DropDownItems.OfType(Of ToolStripDropDownItem)
            If Not ze.Name = MainF.tscm.Name And Not ze.Name = MainF.ts_sm.Name Then
                macro_cb.Items.Add(ze.Text)
            End If
        Next
        tempmacro.AddRange(MainF.macro)
    End Sub

    Private Sub macro_cb_SelectedValueChanged(sender As Object, e As EventArgs) Handles macro_cb.SelectedValueChanged
        t_macro.Clear()
        onamemacro = macro_cb.Text
        t_macro.Text = rmacro(macro_cb.Text)
        Button4.Enabled = True
    End Sub
    Function rmacro(ByVal zz As String)
        Dim rtext As String
        For Each ze As String In MainF.returnmacro(zz, 0)
            If rtext = "" Then rtext = ze Else rtext = rtext & vbNewLine & ze
        Next
        Return rtext
    End Function
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        removemacros(onamemacro)
        Button4.Enabled = False
    End Sub
    Sub removemacros(ByVal z As String)
        macro_cb.Items.Remove(z)
        macro_cb.Text = ""
        t_macro.Clear()
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        tempmacro.Clear()
        For Each item As String In macro_cb.Items
            tempmacro.AddRange(rmacro(item).split(vbNewLine))
        Next
        MainF.macro.Clear()
        MainF.macro.AddRange(tempmacro)
        MainF.refreshmacros()
        Me.Close()
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class