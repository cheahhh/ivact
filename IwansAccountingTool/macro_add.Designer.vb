﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class macro_add
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mc_name = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.MaterialRaisedButton1 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.SuspendLayout()
        '
        'mc_name
        '
        Me.mc_name.Depth = 0
        Me.mc_name.Hint = "Macro Name"
        Me.mc_name.Location = New System.Drawing.Point(12, 12)
        Me.mc_name.MaxLength = 32767
        Me.mc_name.MouseState = MaterialSkin.MouseState.HOVER
        Me.mc_name.Name = "mc_name"
        Me.mc_name.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.mc_name.SelectedText = ""
        Me.mc_name.SelectionLength = 0
        Me.mc_name.SelectionStart = 0
        Me.mc_name.Size = New System.Drawing.Size(154, 23)
        Me.mc_name.TabIndex = 2
        Me.mc_name.TabStop = False
        Me.mc_name.UseSystemPasswordChar = False
        '
        'MaterialRaisedButton1
        '
        Me.MaterialRaisedButton1.Depth = 0
        Me.MaterialRaisedButton1.Location = New System.Drawing.Point(172, 12)
        Me.MaterialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton1.Name = "MaterialRaisedButton1"
        Me.MaterialRaisedButton1.Primary = True
        Me.MaterialRaisedButton1.Size = New System.Drawing.Size(75, 23)
        Me.MaterialRaisedButton1.TabIndex = 3
        Me.MaterialRaisedButton1.Text = "Create"
        Me.MaterialRaisedButton1.UseVisualStyleBackColor = True
        '
        'macro_add
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(261, 48)
        Me.Controls.Add(Me.MaterialRaisedButton1)
        Me.Controls.Add(Me.mc_name)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "macro_add"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Create Macro"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents mc_name As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents MaterialRaisedButton1 As MaterialSkin.Controls.MaterialRaisedButton
End Class
