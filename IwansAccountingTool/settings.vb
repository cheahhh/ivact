﻿Imports System.Net
Imports System.Net.Sockets
Imports System.Threading

Public Class settings
    Private Sub settings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        t_file.Text = My.Settings.defaultfile
        MaterialFlatButton1.Select()
        'c_ne.Checked = My.Settings.ne
        dg.Enabled = c_ne.Checked
    End Sub

    Private Sub MaterialFlatButton1_Click(sender As Object, e As EventArgs) Handles MaterialFlatButton1.Click
        Dim dlg As New OpenFileDialog
        If dlg.ShowDialog = DialogResult.OK Then
            t_file.Text = dlg.FileName
        End If
    End Sub

    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton1.Click
        My.Settings.defaultfile = t_file.Text
        'My.Settings.ne = c_ne.Checked
        My.Settings.Save()
        Me.Close()
    End Sub
    Public Sub addnewpeer(ByVal ip As String)
        sendmessage(ip, "[ADDIP]" & l_myip.Text & "]" & c_part.Checked)
    End Sub
    Private Sub MaterialFlatButton3_Click(sender As Object, e As EventArgs) Handles MaterialFlatButton3.Click
        t_file.Clear()
    End Sub

    Private Sub c_ne_CheckedChanged(sender As Object, e As EventArgs) Handles c_ne.CheckedChanged
        dg.Enabled = c_ne.Checked
        If c_ne.Checked = True Then
            Dim ListenerThread As New Thread(New ThreadStart(AddressOf MainF.listening))
            ListenerThread.Start()
            MainF.t_netw.Enabled = True
            Dim entry = Dns.GetHostEntry(System.Net.Dns.GetHostName())
            For Each address In entry.AddressList
                If address.AddressFamily = AddressFamily.InterNetwork Then
                    l_myip.Text = "IP: " & address.ToString
                    myip = address.ToString
                    Exit For
                End If
            Next
        Else
            MainF.t_netw.Enabled = False
            l_myip.Text = "IP:"
        End If

    End Sub

    Private Sub AddPeerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddPeerToolStripMenuItem.Click
        addpeer.Show()
    End Sub

    Private Sub settings_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        e.Cancel = True
        Me.Hide()
    End Sub

    Private Sub MaterialRaisedButton2_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton2.Click
        f_plog.Show()
    End Sub

    Private Sub RemovePeerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemovePeerToolStripMenuItem.Click
        dg.Rows.Remove(dg.SelectedRows(0))

    End Sub
End Class