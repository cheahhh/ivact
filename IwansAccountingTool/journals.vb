﻿Public Class journals
    Private Sub EditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EditToolStripMenuItem1.Click
        AddJ.Show()
        AddJ.editmode = True
        AddJ.sender = Me.Name
        AddJ.Button3.Text = "Edit"
        AddJ.Text = "Edit Journal Entry"
        AddJ.l_j.Text = "Journal No: " & Me.Name
        AddJ.k_open.Visible = False
        For i As Integer = 0 To dg.RowCount - 1
            If dg.Rows(i).Cells(3).Value = "" And dg.Rows(i).Cells(2).Value <> "" Then
                If Not dg.Rows(i).Cells(0).Value = " " Then AddJ.dtp.Value = dg.Rows(i).Cells(0).Value
                AddJ.dg_d.Rows.Add(New String() {dg.Rows(i).Cells(1).Value, dg.Rows(i).Cells(2).Value})
            End If
            If dg.Rows(i).Cells(2).Value = "" And dg.Rows(i).Cells(3).Value <> "" Then
                If Not dg.Rows(i).Cells(0).Value = " " Then AddJ.dtp.Value = dg.Rows(i).Cells(0).Value
                AddJ.dg_c.Rows.Add(New String() {dg.Rows(i).Cells(1).Value, dg.Rows(i).Cells(3).Value})
            End If
            If dg.Rows(i).Cells(2).Value = "" And dg.Rows(i).Cells(3).Value = "" Then
                Dim z As String = dg.Rows(i).Cells(1).Value.replace("(", "")
                AddJ.t_desc.Text = z.Replace(")", "")
            End If
        Next
    End Sub
    Function dumpcode()
        Dim dc As String = "<J>" & Me.Name
        For i As Integer = 0 To dg.RowCount - 1
            If dg.Rows(i).Cells(3).Value = "" And dg.Rows(i).Cells(2).Value <> "" Then
                dc = dc & vbNewLine & "<D>" & dg.Rows(i).Cells(0).Value & "/" & dg.Rows(i).Cells(1).Value & "/" & dg.Rows(i).Cells(2).Value
            End If
            If dg.Rows(i).Cells(2).Value = "" And dg.Rows(i).Cells(3).Value <> "" Then
                dc = dc & vbNewLine & "<C>" & dg.Rows(i).Cells(0).Value & "/" & dg.Rows(i).Cells(1).Value & "/" & dg.Rows(i).Cells(3).Value
            End If
            If dg.Rows(i).Cells(2).Value = "" And dg.Rows(i).Cells(3).Value = "" Then
                Dim z As String = dg.Rows(i).Cells(1).Value.replace("(", "")
                dc = dc & vbNewLine & "<DE>" & z.Replace(")", "")
            End If
        Next
        dc = dc & vbNewLine & "</J>"
        Return dc
    End Function

    Private Sub RemoveToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemoveToolStripMenuItem1.Click
        If MsgBox("Are you sure?", MsgBoxStyle.OkCancel, "Remove journal entry?") = MsgBoxResult.Ok Then
            MainF.field.Controls.Remove(Me)
        End If
    End Sub

    Private Sub JNo_Click(sender As Object, e As EventArgs) Handles JNo.Click
        dg.ClearSelection()
    End Sub

    Private Sub CreateMacroToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CreateMacroToolStripMenuItem1.Click
        Dim dc As String
        For i As Integer = 0 To dg.RowCount - 1
            If dg.Rows(i).Cells(3).Value = "" And dg.Rows(i).Cells(2).Value <> "" Then
                dc = dc & vbNewLine & "<MD>%DATE%/" & dg.Rows(i).Cells(1).Value & "/" & dg.Rows(i).Cells(2).Value
            End If
            If dg.Rows(i).Cells(2).Value = "" And dg.Rows(i).Cells(3).Value <> "" Then
                dc = dc & vbNewLine & "<MC>%DATE%/" & dg.Rows(i).Cells(1).Value & "/" & dg.Rows(i).Cells(3).Value
            End If
            If dg.Rows(i).Cells(2).Value = "" And dg.Rows(i).Cells(3).Value = "" Then
                Dim z As String = dg.Rows(i).Cells(1).Value.replace("(", "")
                dc = dc & vbNewLine & "<MDE>" & z.Replace(")", "")
            End If
        Next
        dc = dc & vbNewLine & "</M>"
        macro_add.Show()
        macro_add.macrostring = dc
    End Sub
End Class
